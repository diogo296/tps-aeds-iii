#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main() {
	FILE *dictionary, *newDictionary, *input, *stopwords;
	int dictionarySize, inputSize, numErrors, numStopwords;
	int i;
	char string[500];

	dictionary = fopen("dictionary0.txt", "r");
	newDictionary = fopen("dictionary.txt", "w");
	input = fopen("input.txt", "w");
	stopwords = fopen("stopwords.txt", "w");

	printf("Tamanho do dicionario: ");
	scanf("%d", &dictionarySize);

	printf("Numero de stopwords: ");
	scanf("%d", &numStopwords);

	for (i = 0; i < dictionarySize; i++) {
		fscanf(dictionary, "%s", string);

		fprintf(newDictionary, "%s", string);
		if (i != dictionarySize-1) {
			fprintf(newDictionary, "\n");
		}

		if (numStopwords > 0) {
			fprintf(stopwords, "%s", string);
			numStopwords--;
		}
		if (numStopwords > 0) {
			fprintf(stopwords, "\n");
		}
	}

	printf("Tamanho do arquivo texto: ");
	scanf("%d", &inputSize);

	printf("Numero de palavras erradas: ");
	scanf("%d", &numErrors);

	for (i = 0; i < dictionarySize-numErrors; i++) {
		fprintf(input, "%s", string);
		if (i != dictionarySize-numErrors-1) {
			fprintf(input, " ");
		}
	}


	string[strlen(string)-1] = '\0';
	if (i > 0) {
		fprintf(input, " ");
	}

	for (i = 0; i < numErrors; i++) {
		fprintf(input, "%s", string);
		if (i != numErrors-1) {
			fprintf(input, " ");
		}
	}

	fclose(dictionary);
	fclose(newDictionary);
	fclose(input);
	fclose(stopwords);

	return 0;
}
