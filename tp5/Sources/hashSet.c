#include "hashSet.h"

void hashConstructor(HashSet *hashSet) {
	int i;

	hashSet->size = 0;
	hashSet->capacity = 26;

	for (i = 0; i < hashSet->capacity; i++) {
		ListConstructor(&hashSet->table[i]);
	}
}

int hash(typeKey key) {
	int caracterA = 'a';		      // Valor inteiro do caracter 'a'.
	int keyFirstCaracter = key[0];    // Valor inteiro do primeiro caracter da string.

	return (keyFirstCaracter % caracterA);
}

void hashInsert(HashSet *hashSet, nodeKey key) {
    int i = hash(key.word);
    pushBack(key, &hashSet->table[i]);
    hashSet->size++;
}

node *hashFind(HashSet hashSet, typeKey word) {
	int i = hash(word);
	node *key = find(word, hashSet.table[i].end);

	if (key != hashSet.table[i].end) {
		return key;
	}
	else {
		return hashEnd(hashSet);
	}
}

node *hashEnd(HashSet hashSet) {
	return hashSet.table[hashSet.capacity-1].end;
}

node *hashFront(HashSet hashSet) {
	int i = 0;

	for (i = 0; i < hashSet.capacity; i++) {
		if (!empty(hashSet.table[i].size)) {
			return front(hashSet.table[i].end);
		}
	}

	return hashEnd(hashSet);
}

node *hashNext(HashSet hashSet, node *x) {
	int i = hash(x->key.word);

	if (x->next != hashSet.table[i].end) {
		return x->next;
	}

	else {
		for (i = i+1; i < hashSet.capacity; i++) {
			if (!empty(hashSet.table[i].size)) {
				return front(hashSet.table[i].end);
			}
		}
		return hashEnd(hashSet);
	}
}

void clearHash(HashSet *hashSet) {
	int i;

	for (i = 0; i < hashSet->capacity; i++) {
		clearList(&hashSet->table[i]);
	}

	hashSet->size = 0;
}
