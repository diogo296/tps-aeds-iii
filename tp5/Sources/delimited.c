#include "delimited.h"

_Bool isDelimited(char c) {
	int i, stringlen = strlen(DELIMITED);

	for (i = 0; i < stringlen; i++) {
		if (DELIMITED[i] == c) {
			return true;
		}
	}

	return false;
}
