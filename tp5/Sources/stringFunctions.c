#include "stringFunctions.h"

void stringLower(typeKey string) {
	int i;

	for (i = 0; i < strlen(string); i++) {
		string[i] = tolower(string[i]);
	}
}

void stringUpper(typeKey string) {
	int i;

	for (i = 0; i < strlen(string); i++) {
		string[i] = toupper(string[i]);
	}
}

int getStringType(typeKey string) {
	typeKey aux;

	strncpy(aux, string, strlen(string));

	if (aux[0] == tolower(aux[0])) {
		return firstLetterLower;
	}

	aux[0] = string[0];
	stringUpper(aux);

	if (strncmp(string, aux, strlen(string)) == 0) {
		return allUpper;
	}

	else {
		return firstLetterUpper;
	}
}

void backupString(typeKey string, int stringType) {
	if (stringType == firstLetterUpper) {
		string[0] = toupper(string[0]);
	}

	else if (stringType == allUpper) {
		stringUpper(string);
	}
}
