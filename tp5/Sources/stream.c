#include "stream.h"

void openFiles(FILE **input, FILE **output, FILE **dictionary, FILE **stopwords, int argc, char *argv[]) {
	char options[] = "i:d:s:o:";
	int option;

	while((option = getopt(argc, argv, options)) != -1) {
		switch(option) {
			case 'i':
		    	*input = fopen(optarg, "r");
	            break;
			case 'd':
				*dictionary = fopen(optarg, "r");
	        	break;
			case 's':
				*stopwords = fopen(optarg, "r");
	        	break;
			case 'o':
				*output = fopen(optarg, "w");
	        	break;
		}
	}
}

void checkFiles(FILE *input, FILE *dictionary, FILE *stopwords, FILE *output) {
     if (!input || !dictionary || !stopwords || !output) {
    	 printf("Arquivo nao encontrado!\n");
    	 exit(1);
    }
}

void closeFiles(FILE *input, FILE *dictionary, FILE *stopwords, FILE *output) {
	fclose(input);
	fclose(dictionary);
	fclose(stopwords);
	fclose(output);
}

// Lê uma palavra de um arquivo.
void getWord(FILE *file, nodeKey *key) {
	fscanf(file, "%s", key->word);
	key->stopwordFlag = false;
}

// Retorna verdadeiro se o caracter esta delimitado e falso caso contrário.
_Bool setCharFlag(char c) {
	if (isDelimited(c)) {
		return true;
	}
	else {
		return false;
	}
}

void getInputText(FILE *input, List *text) {
	int i;
	_Bool charFlag, newCharFlag;
	char c;
	nodeKey key;

	ListConstructor(text);

	fscanf(input, "%c",	 &key.word[0]);
	charFlag = setCharFlag(key.word[0]);

	while (!feof(input)) {
		for (i = 1; i < WORD_MAX_SIZE; i++) {
			fscanf(input, "%c", &c);
			newCharFlag = setCharFlag(c);

			if ((charFlag == newCharFlag) && !feof(input)) {
				key.word[i] = c;
			}
			else {
				key.word[i] = '\0';
				pushBack(key, text);
				break;
			}
		}

		key.word[0] = c;
		charFlag = newCharFlag;
	}
}

void getAllWords(FILE *dictionary, HashSet *hashSet) {
	nodeKey key;

	getWord(dictionary, &key);

	while (!feof(dictionary)) {
		hashInsert(hashSet, key);
		getWord(dictionary, &key);
	}
}

void setStopWords(FILE *stopwords, HashSet *dictionary) {
	nodeKey key;
	node *aux;

	getWord(stopwords, &key);

	while (!feof(stopwords)) {
		aux = hashFind(*dictionary, key.word);
		aux->key.stopwordFlag = true;
		getWord(stopwords, &key);
	}
}

// Retorna o número de caracteres iguais à quebra de linha ou tabulação presentes em uma string.
int numNonCharacters(char string[]) {
	int i, strlenght = strlen(string);
	int total = 0;

	for (i = 0; i < strlenght; i++) {
		if ((string[i] == '\n') || (string[i] == '\t') || (string[i] == '\r')) {
			total++;
		}
	}

	return total;
}

// Escreve no arquivo de saída o número de caracteres do texto corrigido.
void writeNumCharacters(List text, FILE *output) {
	node *i;
	int numCharacters = 0;

	for (i = front(text.end); i != text.end; i = i->next) {
		numCharacters += strlen(i->key.word) - numNonCharacters(i->key.word);
	}

	fprintf(output, "%d\n", numCharacters);
}

// Escreve no arquivo de saída o número de palavras do texto corrigido.
void writeNumWords(List text, FILE *output) {
	node *i;
	int numWords = 0;

	for (i = front(text.end); i != text.end; i = i->next) {
		if (!isDelimited(i->key.word[0])) {
			numWords++;
		}
	}

	fprintf(output, "%d\n", numWords);
}

// Escreve no arquivo de saída um número inteiro.
void writeIntValue(int value, FILE *output) {
	fprintf(output, "%d\n", value);
}

// Escreve no arquivo de saída as palavras mais frequêntes do texto corrigido.
void writeTopWords(HashSet allWords, FILE *output) {
	Heap heap;
	int i;

	constructHeap(&heap, allWords);

	heapSort(&heap);

	for (i = 0; i < heap.size; i++) {
		if (i == 20) {
			break;
		}
		fprintf(output, "%s ", heap.vector[i].word);
		writeIntValue(heap.vector[i].count, output);
	}

	clearHeap(&heap);
}

// Escreve no arquivo de saída o texto corrigido.
void writeRevisedText(List text, FILE *output) {
	node *i;

	fprintf(output, "#Texto\n");

	for (i = front(text.end); i != text.end; i = i->next) {
		fprintf(output, "%s", i->key.word);
	}
}

void writeOutput(FILE *output, List text, HashSet allWords, int numRevisedWords, int numDraws) {
	writeNumCharacters(text, output);
	writeNumWords(text, output);
	writeTopWords(allWords, output);
	writeIntValue(numRevisedWords, output);
	writeIntValue(numDraws, output);
	writeRevisedText(text, output);
}
