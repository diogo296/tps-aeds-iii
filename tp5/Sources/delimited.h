#include <stdbool.h>
#include <string.h>

#ifndef DELIMITERS_H_
#define DELIMITERS_H_

#define DELIMITED " \n\t\r.,:;/\"+_(){}[]<>*&^%$@#§£¢¬!?~¨´`/|\\=1234567890¹²³⁴⁵⁶⁷⁸⁹⁰"

// Retorna verdadeiro se o caracter está delimitado.
_Bool isDelimited(char c);

#endif /* DELIMITERS_H_ */
