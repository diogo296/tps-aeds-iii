#include <stdbool.h>
#include <limits.h>

#include "stream.h"
#include "matrix.h"
#include "delimited.h"
#include "stringFunctions.h"

#ifndef SPELLCHECKER_H_
#define SPELLCHECKER_H_

// Corretor ortográfico.
void spellChecker(FILE *input, FILE *dictionary, FILE *stopwords, FILE *output);

#endif /* SPELLCHECKER_H_ */
