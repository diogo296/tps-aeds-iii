#include "node.h"

node *NewSentinel() {
    node *aux = (node*) malloc(sizeof(node));
    aux->next = aux->prev = aux;
    return aux;
}

node *NewNode(nodeKey key, node *left, node *right) {
    node *aux = (node*) malloc(sizeof(node));
    aux->key = key;
    aux->key.count = 0;
    aux->prev = left;
    aux->next = right;
    return aux;
}
