#include "matrix.h"

void constructMatrix(Matrix *matrix, int numLines, int numColumns) {
	int i;

	matrix->numLines = numLines;
	matrix->numColums = numColumns;

	matrix->array = (matrixCel**) malloc(sizeof(matrixCel*) * (numLines + 1));

	for (i = 0; i <= numLines; i++) {
		matrix->array[i] = (matrixCel*) malloc(sizeof(matrixCel) * (numColumns + 1));
	}

	for (i = 0; i <= numLines; i++) {
		matrix->array[i][0].cost = i;
		matrix->array[i][0].operation = DELETION;
	}

	for (i = 0; i <= numColumns; i++) {
		matrix->array[0][i].cost = i;
		matrix->array[0][i].operation = INSERTION;
	}
}

void freeMatrix(Matrix matrix) {
	int i;

	for (i = 0; i <= matrix.numLines; i++) {
		free(matrix.array[i]);
	}

	free(matrix.array);
}
