#include "spellChecker.h"

// Escolhe a operação de menor custo e guarda a operação realizada.
void minimum(int insertionValue, int substitutionValue, int deletionValue,
		int transpositionValue, int cost, matrixCel *distanceMatrix, int *lastStrlen, int strlen2) {
	distanceMatrix->cost = substitutionValue;
	distanceMatrix->operation = SUBSTITUTION;
	if (cost == 0) {
		distanceMatrix->operation = MATCH;
	}

	if (transpositionValue < distanceMatrix->cost) {
		distanceMatrix->cost = transpositionValue;
		distanceMatrix->operation = TRANSPOSITION;
	}

	if (((insertionValue == distanceMatrix->cost) && (*lastStrlen < strlen2)) || (insertionValue < distanceMatrix->cost)) {
		distanceMatrix->cost = insertionValue;
		distanceMatrix->operation = INSERTION;
		*lastStrlen += 1;
	}

	if (((deletionValue == distanceMatrix->cost) && (*lastStrlen > strlen2)) || (deletionValue < distanceMatrix->cost)) {
		distanceMatrix->cost = deletionValue;
		distanceMatrix->operation = DELETION;
		*lastStrlen -= 1;
	}
}

// Retorna o custo de transposição.
int transpositionValue(matrixCel **distanceMatrix, int i, int j, typeKey string1, typeKey string2, int cost) {
	if ((i > 1) && (j > 1) && (string1[i-1] == string2[j-2]) && (string1[i-2] == string2[j-1])) {
		return distanceMatrix[i-2][j-2].cost + cost;
	}

	else {
		return INT_MAX;
	}
}

// Calcula a distância de edição das strings 1 e 2 nas posições i e j, respectivamente.
void computeDistance(matrixCel **distanceMatrix, int i, int j, typeKey string1, typeKey string2, int *lastStrlen) {
	int deletion, insertion, substitution, transposition, cost;

	cost = (string1[i-1] != string2[j-1]);

	insertion = distanceMatrix[i][j-1].cost + 1;
	substitution = distanceMatrix[i-1][j-1].cost + cost;
	deletion = distanceMatrix[i-1][j].cost + 1;
	transposition = transpositionValue(distanceMatrix, i, j, string1, string2, cost);

	minimum(insertion, substitution, deletion, transposition, cost, &distanceMatrix[i][j], lastStrlen, strlen(string2));
}

// Calcula a distância de edição entre duas strings.
void editDistance(typeKey string1, typeKey string2, matrixCel **distanceMatrix) {
	int i, j;
	int strlen1, strlen2, lastStrlen;

	strlen1 = strlen(string1);
	strlen2 = strlen(string2);
	lastStrlen = strlen1;

	for (i = 1; i <= strlen1; i++) {
		for (j = 1; j <= strlen2; j++) {
			computeDistance(distanceMatrix, i, j, string1, string2, &lastStrlen);
		}
	}
}

// Retorna a prioridade das operações.
int computeOperationsPriority(Matrix distanceMatrix) {
	int i = distanceMatrix.numLines, j = distanceMatrix.numColums, total = 0;

	while ( (i >= 0 && j > 0) || (i > 0 && j >= 0) ) {
		switch (distanceMatrix.array[i][j].operation) {
			case INSERTION: {
				total += INSERTION;
				j--;
				break;
			}
			case SUBSTITUTION: {
				total += SUBSTITUTION;
				i--;
				j--;
				break;
			}
			case DELETION: {
				total += DELETION;
				i--;
				break;
			}
			case TRANSPOSITION: {
				total += TRANSPOSITION;
				i -= 2;
				j -= 2;
				break;
			}
			case MATCH: {
				i--;
				j--;
				break;
			}
		}
	}

	return total;
}

// Retorna o nó que contém a palavra de maior preferência.
node *higherPreferenceNode(Matrix distanceMatrix, node *dictionaryWord,
		node *lowerNode, int *lastPriority, int *lastEditDistance, _Bool *drawFlag) {
	int numLines, numColumns, dictionaryWordPriority;

	numLines = distanceMatrix.numLines;
	numColumns = distanceMatrix.numColums;

	if (distanceMatrix.array[numLines][numColumns].cost <= 2) {    // Distância de edição menor ou igual a 2.
		dictionaryWordPriority = computeOperationsPriority(distanceMatrix);

		// Se a distância de edição da palavra do dicionário for menor:
		if (distanceMatrix.array[numLines][numColumns].cost < *lastEditDistance) {
			*drawFlag = false;
			*lastEditDistance = distanceMatrix.array[numLines][numColumns].cost;
			*lastPriority = dictionaryWordPriority;
			return dictionaryWord;
		}

		else if (distanceMatrix.array[numLines][numColumns].cost == *lastEditDistance) {
			*drawFlag = true;
			// Se a palavra do dicionário possui prioridade de operações ou prioridade igual mas é alfabeticamente menor:
			if ( (dictionaryWordPriority > *lastPriority) || ((dictionaryWordPriority == *lastPriority)
				  && (strncmp(dictionaryWord->key.word, lowerNode->key.word, WORD_MAX_SIZE) < 0)) ) {
				*lastPriority = dictionaryWordPriority;
				return dictionaryWord;
			}
		}
	}

	return lowerNode;
}

// Retorna o nó do dicionário que contém a palavra de menor distância de edição.
node *lowerEditDistanceNode(HashSet hashSet, typeKey word, _Bool *drawFlag) {
	Matrix distanceMatrix;
	node *i, *lowerNode;
	int strlen1, lastPriority = 0, lastEditDistance = INT_MAX;

	*drawFlag = false;
	lowerNode = hashFind(hashSet, word);

	// Caso a palavra seja encontrada no dicionário:
	if (lowerNode != hashEnd(hashSet)) {
		return lowerNode;
	}

	else {
		strlen1 = strlen(word);
		strncpy(lowerNode->key.word, word, WORD_MAX_SIZE);

		for (i = hashFront(hashSet); i != hashEnd(hashSet); i = hashNext(hashSet, i)) {
			constructMatrix(&distanceMatrix, strlen1, strlen(i->key.word));

			// Calcula a distância de edição entre a palavra do texto e a palavra do dicionário do nó i.
			editDistance(word, i->key.word, distanceMatrix.array);

			// Escolhe o nó de maior prioridade entre a atual palavra do dicionário e a última palavra escolhida.
			lowerNode = higherPreferenceNode(distanceMatrix, i, lowerNode, &lastPriority, &lastEditDistance, drawFlag);

			freeMatrix(distanceMatrix);
		}
	}

	return lowerNode;
}

// Atualiza o número de palavras corrigidas, o número de empates e o contador da palavra.
void updateTextMetrics(int *numRevisedWords, int *numDraws, typeKey string,
		node *revisedString, _Bool drawFlag) {
	// Atualiza número de palavras corrigidas.
	if (strncmp(string, revisedString->key.word, WORD_MAX_SIZE) != 0) {
		*numRevisedWords += 1;
	}

	//Atualiza número de empates.
	if (drawFlag) {
		*numDraws += 1;
	}

	// Atualiza o contador da palavra.
	if (revisedString->key.stopwordFlag == false) {
		revisedString->key.count++;
	}
}

void spellChecker(FILE *input, FILE *dictionary, FILE *stopwords, FILE *output) {
	HashSet allWords;
	List text;
	int stringType, numDraws = 0, numRevisedWords = 0;
	_Bool drawFlag;
	node *lowerNode, *aux;

	hashConstructor(&allWords);

	// Guarda o dicionário em uma tabela hash.
	getAllWords(dictionary, &allWords);
	setStopWords(stopwords, &allWords);

	// Guarda o texto de entrada em uma lista encadeada.
	getInputText(input, &text);

	// Para cada palavra do texto:
	for (aux = front(text.end); aux != text.end; aux = aux->next) {
		// Se a palavra não é composta por caracteres delimitados:
		if (!isDelimited(aux->key.word[0])) {
			// Guarda o tipo da palavra e reduz todos as suas letras para minúsculo.
			stringType = getStringType(aux->key.word);
			stringLower(aux->key.word);

			// Encontra o nó do dicionário que contem a palavra de menor distância de edição e maior prioridade.
			lowerNode = lowerEditDistanceNode(allWords, aux->key.word, &drawFlag);

			updateTextMetrics(&numRevisedWords, &numDraws, aux->key.word, lowerNode, drawFlag);

			// Retorna a palavra para o formato original.
			strncpy(aux->key.word, lowerNode->key.word, WORD_MAX_SIZE);
			backupString(aux->key.word, stringType);
		}
	}

	writeOutput(output, text, allWords, numRevisedWords, numDraws);

	clearHash(&allWords);
	clearList(&text);
}
