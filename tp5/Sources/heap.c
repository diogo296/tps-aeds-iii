#include "heap.h"

void constructHeap(Heap *heap, HashSet allWords) {
	node *i;

	heap->vector = (nodeKey*) malloc(sizeof(nodeKey) * allWords.size);
	heap->size = 0;

	for (i = hashFront(allWords); i != hashEnd(allWords); i = hashNext(allWords, i)) {
		if (!isDelimited(i->key.word[0]) && (i->key.stopwordFlag == false) && (i->key.count > 0)) {
			minHeapInsert(heap, i->key);
		}
	}
}

// Retorna a posição elemento à esquerda da árvore i.
int left(int i) {
	return (2*i + 1);
}

// Retorna a posição elemento à direita da árvore i.
int right(int i) {
	return (2*i + 2);
}

// Retorna a posição elemento à direita da árvore i.
int parent(int i) {
	return ((i-1)/2);
}

// Troca o valor de dois elementos.
void swap(nodeKey *heapVector, int i, int j) {
	nodeKey aux;
	aux = heapVector[i];
	heapVector[i] = heapVector[j];
	heapVector[j] = aux;
}

// Retorna verdadeiro se a chave key1 é maior que a chave key2 segundo os critérios de ordenação.
_Bool isHigher(nodeKey key1, nodeKey key2) {
	if (key1.count == key2.count) {
		return (strncmp(key1.word, key2.word, WORD_MAX_SIZE) < 0);
	}
	else {
		return (key1.count > key2.count);
	}
}

// Retorna verdadeiro se a chave key1 é menor que a chave key2 segundo os critérios de ordenação.
_Bool isLower(nodeKey key1, nodeKey key2) {
	if (key1.count == key2.count) {
		return (strncmp(key1.word, key2.word, WORD_MAX_SIZE) > 0);
	}
	else {
		return (key1.count < key2.count);
	}
}

void minHeap(Heap *heap, int i) {
	int head;
	int l = left(i);
	int r = right(i);

	if ((l < heap->size) && isLower(heap->vector[l], heap->vector[i])) {
		head = l;
	}
	else {
		head = i;
	}

	if ((head < heap->size) && (r < heap->size) && isLower(heap->vector[r], heap->vector[head])) {
		head = r;
	}

	if (head != i) {
		swap(heap->vector, i, head);
		minHeap(heap, head);
	}
}

// Retorna e exclui o menor elemento do heap.
void heapSwapMin(Heap *heap) {
	swap(heap->vector, 0, heap->size-1);
	heap->size--;
	minHeap(heap, 0);
}

// Mantém a propriedade do heap mínimo para a subárvore i.
void heapIncreaseKey(Heap *heap, int i, nodeKey key) {
	heap->vector[i] = key;

	while ((i >= 1) && (isHigher(heap->vector[parent(i)], heap->vector[i]))) {
		swap(heap->vector, parent(i), i);
		i = parent(i);
	}
}

// Insere um elemento no heap.
void minHeapInsert(Heap *heap, nodeKey key) {
	heap->size++;
	heapIncreaseKey(heap, heap->size-1, key);
}

void heapSort(Heap *heap) {
	int size = heap->size;

	while (heap->size > 1) {
		heapSwapMin(heap);
	}

	heap->size = size;
}

void clearHeap(Heap *heap) {
	free(heap->vector);
	heap->size = 0;
}
