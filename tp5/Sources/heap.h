#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "node.h"
#include "hashSet.h"
#include "delimited.h"

#ifndef HEAP_H_
#define HEAP_H_

typedef struct {
	nodeKey *vector;
	int size;
} Heap;

// Cria um heap.
void constructHeap(Heap *heap, HashSet allWords);

// Insere um elemento no heap.
void minHeapInsert(Heap *heap, nodeKey key);

// Ordena um conjunto na ordem de complexidade O(nlogn).
void heapSort(Heap *heap);

// Libera a memória alocada pelo heap.
void clearHeap(Heap *heap);

#endif /* HEAP_H_ */
