#include "list.h"

void ListConstructor(List *list) {
    list->end = NewSentinel();
    list->size = 0;
}

node *front(node *listEnd) {
    return listEnd->next;
}

node *back(node *listEnd) {
    return listEnd->prev;
}

void last(node *listEnd, nodeKey key) {
    key = back(listEnd)->key;
}

node *find(typeKey key, node *listEnd) {
    node *aux;

    for (aux = front(listEnd); aux != listEnd; aux = aux->next) {
        if (strncmp(key, aux->key.word, WORD_MAX_SIZE) == 0) {
            return aux;
        }
    }

    // Se o nó não for encontrado:
    return listEnd;
}

void popFront(List* list) {
    node *aux = front(list->end);

    aux->next->prev = aux->prev;
    aux->prev->next = aux->next;

    free(aux);
    list->size--;
}

void pushBack(nodeKey key, List *list) {
    node *aux = NewNode(key, back(list->end), list->end);
    back(list->end)->next = aux;
    list->end->prev = aux;
    list->size++;
}

_Bool empty(int listSize) {
    return (listSize == 0);
}

void clearList(List *list) {
    while ( !empty(list->size) ) {
        popFront(list);
    }
    free(list->end);
}
