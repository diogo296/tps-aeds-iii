#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

#include "hashSet.h"
#include "delimited.h"
#include "heap.h"
#include "stringFunctions.h"

#define WORD_MAX_SIZE 500

#ifndef STREAM_H_INCLUDED
#define STREAM_H_INCLUDED

// Abre os arquivos.
void openFiles(FILE **input, FILE **output, FILE **dictionary, FILE **stopwords, int argc, char *argv[]);

// Termina o programa caso um dos arquivos não seja aberto corretamente.
void checkFiles(FILE *input, FILE *dictionary, FILE *stopwords, FILE *output);

// Fecha os arquivos.
void closeFiles(FILE *input, FILE *dictionary, FILE *stopwords, FILE *output);

// Armazena o texto de entrada em uma lista encadeada.
void getInputText(FILE *input, List *text);

// Lê todas as palavras do dicionário.
void getAllWords(FILE *dictionary, HashSet *hashSet);

// Marca as stopwords na tabela hash que armazena as palavras do dicionário.
void setStopWords(FILE *stopwords, HashSet *dictionary);

// Escreve no arquivo de saída todas as métricas de avaliação do texto e o texto corrigido.
void writeOutput(FILE *output, List text, HashSet allWords, int numRevisedWords, int numDraws);

#endif // STREAM_H_INCLUDED
