#include <stdio.h>
#include "node.h"

#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED

typedef struct {
    node *end;    // Nó marcador do fim da lista.
    int size;     // Número de elementos da lista.
} List;

// Inicializa uma lista vazia.
void ListConstructor(List *list);

// Retorna um ponteiro para o primeiro nó da lista em O(1).
node *front(node *listEnd);

// Retorna um ponteiro para o último nó da lista em O(1).
node *back(node *listEnd);

// Retorna um vértice de certo key na lista em O(n).
node *find(typeKey key, node *listEnd);

// Remove o primeiro elemento da lista em O(1).
void popFront(List *list);

// Insere um vértice no final da lista em O(1).
void pushBack(nodeKey key, List *list);

// Retorna TRUE caso a lista seja vazia e FALSE caso contrário.
_Bool empty(int listSize);

// Remove todos os elementos da lista em O(n) e libera a memória alocada,
// onde n é o número de elementos na lista.
void clearList(List *list);

#endif // LIST_H_INCLUDED
