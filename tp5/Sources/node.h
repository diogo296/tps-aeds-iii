#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifndef NODE_H_INCLUDED
#define NODE_H_INCLUDED

#define WORD_MAX_SIZE 500

typedef char typeKey[WORD_MAX_SIZE];

typedef struct {
    typeKey word;          // Contém uma palavra do texto.
    _Bool stopwordFlag;    // Indica se a palavra é um stopword ou não.
    int count;		   	   // Contador do número de repetições da palavra no texto.
} nodeKey;

typedef struct Node {
	nodeKey key;		   // Chave do nó da lista.
    struct Node *next;     // Apontador para o próximo nó.
    struct Node *prev;     // Apotador para o nó anterior.
} node;

// Cria o nó sentinela.
node *NewSentinel();

// Cria um novo node.
node *NewNode(nodeKey key, node *left, node *right);

#endif // NODE_H_INCLUDED
