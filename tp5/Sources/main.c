#include <stdlib.h>
#include <stdio.h>

#include "spellChecker.h"

int main(int argc, char *argv[]) {
	FILE *input, *output, *dictionary, *stopwords;

	openFiles(&input, &output, &dictionary, &stopwords, argc, argv);
	checkFiles(input, dictionary, stopwords, output);

	spellChecker(input, dictionary, stopwords, output);

	closeFiles(input, dictionary, stopwords, output);

	return 0;
}
