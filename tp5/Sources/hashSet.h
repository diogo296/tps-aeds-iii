#include "list.h"

#ifndef HASHSET_H_
#define HASHSET_H_

typedef struct {
	int capacity;	   // Número de listas contidas no vetor de listas (uma para cada letra do alfabeto).
	List table[26];    // Vetor de listas.
	int size; 		   // Número de elementos no conjunto.
} HashSet;

// Constrói uma tabela hash composta de 26 listas, uma para cara letra do alfabeto.
void hashConstructor(HashSet *hash);

// Retorna o índice da lista da chave.
int hash(typeKey key);

// Retorna o nó end da última lista da tabela hash.
node *hashEnd(HashSet hashSet);

// Retorna o nó do primeiro elemento da tabela hash.
node *hashFront(HashSet hashSet);

// Retorna o nó do próximo elemento da tabela hash.
node *hashNext(HashSet hashSet, node *x);

// Insere um elemento na tabela hash.
void hashInsert(HashSet *hashSet, nodeKey key);

// Procura um elemnto na tabela hash e retorna seu nó ou a função hashEnd, caso o elemento não pertença ao conjunto.
node *hashFind(HashSet hashSet, typeKey word);

// Libera a memória alocada pela tabela hash.
void clearHash(HashSet *hashSet);

#endif /* HASHSET_H_ */
