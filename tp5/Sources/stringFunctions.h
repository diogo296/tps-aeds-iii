#include <string.h>
#include <ctype.h>

#include "node.h"
#include "delimited.h"

#ifndef STRINGFUNCTIONS_H_
#define STRINGFUNCTIONS_H_

enum { firstLetterLower = 0, firstLetterUpper, allUpper };

// Transforma todos as letras de uma string em letras minúsculas.
void stringLower(typeKey string);

// Transforma todos as letras de uma string em letras maiúsculas.
void stringUpper(typeKey string);

// Retorna o tipo da string, o qual pertence ao conjunto definido pela enumeração acima.
int getStringType(typeKey string);

// Retorna a string ao seu tipo original.
void backupString(typeKey string, int stringType);

#endif /* STRINGFUNCTIONS_H_ */
