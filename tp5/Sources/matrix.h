#include <stdlib.h>
#include <stdio.h>

#ifndef MATRIX_H_
#define MATRIX_H_

typedef struct {
	int cost;
	int operation;
} matrixCel;

typedef struct {
	matrixCel **array;
	int numLines;
	int numColums;
} Matrix;

enum { MATCH = 0, TRANSPOSITION = 1, DELETION = 2, SUBSTITUTION = 4, INSERTION = 8 };

// Aloca memória e inicializa uma matriz.
void constructMatrix(Matrix *matrix, int numLines, int numColumns);

// Libera o espaço alocado por uma matriz.
void freeMatrix(Matrix matrix);

#endif /* MATRIX_H_ */
