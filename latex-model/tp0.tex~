%	Documenta��o do Trabalho Pr�tico 2 de AEDSIII
%	@Diogo Marques Santana
%
%	* Voc� pode identificar erros de grafia atrav�s do seguinte comando linux:
%		aspell --encoding="iso8859-1" -c -t=tex --lang="pt_BR" tp0.tex
%	
%	Tenha cuidado com problemas de codifica��o, voc� pode perder muito tempo com isso (ter que reescrever o texto por que os caracteres % acendutados n�o aparecem corretamento no pdf, por exemplo). Se voc� usa Vi/Vim, ele identifica a codifica��o sozinho, em editores do tipo % Kate/Kwrite voc� pode definir a codifica��o em Ferramentas/Codifica��o, escolha a op��o Oeste Europeu (iso8859-1).
%	Para compilar o texto utilize o comando make (foi criado um Makefile)
%	Para maiores informa��es consulte refer�ncias sobre Latex

\documentclass[12pt]{article}
\usepackage{sbc-template}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{subfigure}
\usepackage{times,amsmath,epsfig}
\usepackage{graphicx,url}
 \makeatletter
 \newif\if@restonecol
 \makeatother
 \let\algorithm\relax
 \let\endalgorithm\relax 
\usepackage[lined,algonl,ruled]{algorithm2e}
%\usepackage{multirow}
\usepackage[brazil]{babel}   
\usepackage[latin1]{inputenc}

\sloppy

\title{Trabalho Pr�tico 3: \\ Par�nteses em uma Express�o Booleana}

\author{Diogo Marques Santana}

\address{Departamento de Ci�ncia da Computa��o -- Universidade Federal de Minas Gerais (UFMG)
\email{diogo.marques@dcc.ufmg.br}
}

\begin{document} 

\maketitle

\begin{resumo} 
  Este trabalho implementa uma solu��o para a contagem do n�mero de formas de coloca��o de par�nteses em uma express�o booleana, de tal forma que a express�o retorne verdadeiro. O algoritmo proposto exp�e uma solu��o para express�es de $t$ termos, com $0 < t < 51$, utilizando o paradigma computacional de Programa��o Din�mica.
\end{resumo}

\section{INTRODU��O}

O problema em quest�o consiste em avaliar as diferentes express�es booleanas obtidas atrav�s das diferentes combina��es de par�nteses e contabilizar o n�mero de express�es que retornam \textit{verdadeiro}.

	Tal problema � definido para uma express�o de $t$ termos, com $0 < t < 51$, para os seguintes operadores: $not$ (nega��o), $and$ (conjun��o), $or$ (disjun��o inclusiva), $xor$ (disjun��o exclusiva), $nand$ (nega��o da conjun��o), $imp$ (implica��o) e $eqv$ (equival�ncia).

	O algoritmo proposto a seguir divide a express�o original at� que a subexpress�o originada seja m�nima, ou seja, at� que restem duas vari�veis e um operador, caso seja um operador bin�rio, ou uma vari�vel e um operador, caso o operador seja o $not$. Logo em seguida, as subexpress�es s�o combinadas e, de acordo com o operador entre elas, � determinado da quantidade de verdadeiros e falsos da nova subexpress�o.

	Em rela��o ao m�todo proposto, � notavel que v�rias subexpress�es s�o avaliadas mais de uma vez. Visando um melhor desempenho do algoritmo, foi utilizado o paradigma de Programa��o Din�mica, uma vez que foi escolhido utilizar uma maior quantidade de mem�ria durante a execu��o do programa, com o objetivo de guardar os resultados das subexpress�es e evitar itera��es repetidas.

\section{REFER�NCIAS RELACIONADAS}
\label{trabalhos_relacionados}

\begin{itemize}

\item \textbf{Literatura:} 
O problema de coloca��o de par�ntesis em uma express�o boolena � um problema recorrente do paradigma da Programa��o Din�mica, visto o conflito entre mem�ria utilizada e desempenho do algoritmo.
\item \textbf{Linguagem C:}
Todo o trabalho foi implementado utilizando a linguagem de programa��o C e compilado utilizando o compilador padr�o GCC (desenvolvido pelo projeto GNU). Mais informa��es sobre a linguagem C podem ser encontradas nas refer�ncias [4] e [5].

\end{itemize}


\section{SOLU��O PROPOSTA}
\label{solucao_proposta}

A seguir ser� detalhada a ideia principal presente no algoritmo, bem como a estrutura de dados e o algoritmo implementado.

\subsection{Fun��o principal} 

A ideia principal consiste em dividir o problema em subproblemas, at� que o caso m�nimo seja alcan�ado, ou seja, duas vari�veis e um operador, no caso de um operador bin�rio, ou uma vari�vel e um operador, no caso do $not$. Resolvidos os casos bases, basta unir as subexprees�es formadas para formar novas express�es e combin�-las de modo a gerar todas as express�es poss�veis na coloca��o de par�nteses.

O algoritmo proposto utiliza duas matrizes $t\times t$, sendo $t$ igual ao n�mero de termos da express�o, para guardar o n�mero de verdadeiros e falsos entre cada subexpress�o, de modo a evitar itera��es repetidas. Portanto, torna-se necess�rio apenas avaliar, em cada combina��o de subexpress�es, a quantidade de verdadeiros e falsos das subexpress�es em quest�o, dado o operador entre elas.

As matrizes sempre cont�m o resultado da express�o entre as posi��es $i$ e $j$. Logo, o resultado do n�mero de express�es que retornam retornam verdadeiro estar� localizado na primeira linha e �ltima coluna da matriz de verdadeiros.

\subsection{Estruturas de dados}

\subsubsection{Express�o booleana} 
A express�o booleana foi armazenada em um vetor de inteiros, sendo definidos as seguintes constantes para cada vari�vel e operador:

\begin{table}[ht!]
\centering
\begin{footnotesize}
\begin{tabular}{|c|c|}
\hline
\textbf{Vari�vel / Operador}	& \textbf{Valor}\\ \hline
False	& 0		\\ \hline
True	& 1		\\ \hline
and	& 2		\\ \hline
or	& 3		\\ \hline
xor	& 4		\\ \hline
nand	& 5		\\ \hline
not	& 6		\\ \hline
imp	& 7		\\ \hline
eqv	& 8		\\ \hline
\end{tabular}
\end{footnotesize}
\caption{Constantes da express�o booleana. \label{tempo_execucao_total}}
\end{table}

O vetor sempre ter� complexidade de espa�o constante ($O(1)$), uma vez que seu tamanho � pr�-definido no algoritmo pelo n�mero m�ximo de termos da express�o (50).

\subsubsection{Matrizes de verdadeiros e falsos} 
Guarda a quantidade de verdadeiros e falsos de todas as subexpress�es meio de duas matrizes $t\times t$, onde $t$ � o n�mero de termos da express�o. Logo, sua complexidade de espa�o � $O(t�)$. Note que, para acessar o n�mero de verdadeiros e falsos da subexpress�o entre $i$ e $j$, basta acessar a posi��o $[i][j]$ das matrizes.


\subsection{Algoritmo}

A seguir ser� detalhado a impelementa��o das principais fun��es do algoritmo.

\subsubsection{Coloca��o de par�nteses na express�o booleana}

Consiste em criar e iniciar as matrizes de verdadeiros e falsos com o valor 0, contar o n�mero de verdadeiros e falsos de cada subexpress�o, combinando seus resultados, e retornar a posi��o da matriz de verdadeiros que cont�m a resposta.

\begin{algorithm}[h!]
\begin{footnotesize}
	criar as matrizes de verdadeiros e falsos\;
	inicializar as matrizes com 0 em todas as posi��es\;
	contar o n�mero de verdadeiros e falsos de todas as subexpress�es\;
	retornar a �ltima posi��o da primeira linha da matriz de verdadeiros\;
\caption{parenthesization(express�o booleana, tamanho da express�o)}%
\end{footnotesize}
\end{algorithm}

Note que criar e inicializar as matrizes tem complexidade $O(t�)$, enquanto contar os verdadeiros e falsos tem complexidade $O(n�)$, sendo $n$ o n�mero de operadores, como veremos a seguir. Portanto, a complexidade final do algoritmo � $O(n�)$.

\subsubsection{Contagem dos verdadeiros e falsos das subexpress�es}

O pseudoc�digo a seguir ilustra o funcionamento da fun��o de contagem de verdadeiros e falsos de uma express�o booleana a partir da contabiliza��o dos verdadeiros e falsos de suas subexpress�es.

\begin{algorithm}[h!]
\begin{footnotesize}
	\If{($i$ == $j$)} {
		determinar a matriz de verdadeiros e falsos de acordo com o valor da vari�vel na posi��o $i$\;
	}
	\Else {
		\For{(cada operador $k$ entre as posi��es $i$ e $j$ da express�o)} {
			\If{((a subexpress�o esquerda n�o foi avaliada) AND (o operador n�o � o $not$))} {
				chamar recursivamente a fun��o para a subexpress�o esquerda\;
			}
			\If{((a subexpress�o direita n�o foi avaliada))} {
				chamar a fun��o recursivamente para a subexpress�o direita\;
			}
			calcular o n�mero de verdadeiros e falsos para a express�o formada entre a subexpress�o esquerda e direita\;
		}
	}
\caption{computeParenthesizations(i, j, matriz de verdadeiros, matriz de falsos, express�o)}%
\end{footnotesize}
\end{algorithm}

Note que a fun��o � iniciada com $i$ igual � posi��o do primeiro termo da express�o e $j$ com a posi��o do �ltimo termo. Al�m disso, a parte esquerda da express�o se encontra entre as posi��es $i$ e $k-1$, enquanto a parte direita � representada de $k+1$ at� $j$, sendo $k$ a posi��o do operador na express�o.

Observe que uma express�o booleana qualuqer tem no m�nimo $(n/2) - 1$ operadores, com $n$ igual ao n�mero de operadores. Al�m do mais, para cada operador, a fun��o � chamada recursivamente para a parte esquerda e direita. Visto que, para cada parte da express�o, temos $n$ outras partes que ser�o chamadas recursivamente, teremos a complexidade final igual a $O(n�)$. O c�lculo do n�mero de verdadeiros e falsos n�o interfere na complexidade final do algoritmo j� que, como veremos a seguir, esta fun��o tem complexidade $O(1)$.

\subsubsection{Computar uma express�o}

Seja $exp1$ a parte esquerda da express�o booleana, de $i$ at� $k-1$, e $exp2$ a parte direita, de $k+1$ at� $j$, com $k$ igual a posi��o de um operador. A fun��o a seguir consiste em computar a subexpress�o entre as posi��es $i$ e $j$ da express�o e guardar o n�mero de falsos e verdadeiros na posi��o $[i][j]$ das matrizes.

\begin{algorithm}[h!]
\begin{footnotesize}
	\Switch{(operador)} {
		\Case {AND:} {
			computar a express�o ($exp1$ AND $exp2$)\;
		}
		\Case {OR:} {
			computar a express�o ($exp1$ OR $exp2$)\;
		}
		\Case {XOR:} {
			computar a express�o ($exp1$ XOR $exp2$)\;
		}
		\Case {NAND:} {
			computar a express�o ($exp1$ NAND $exp2$)\;
		}
		\Case {NOT:} {
			\If{(a subexpress�o entre $i$ e $j$ ainda n�o foi avaliada)} {
				computar a express�o (NOT $exp2$)\;
			}
		}
		\Case {IMP:} {
			computar a express�o ($exp1$ IMP $exp2$)\;
		}
		\Case {EQV:} {
			computar a express�o ($exp1$ EQV $exp2$)\;
		}
	}
\caption{computeExpression(operador, matriz de verdadeiros, matriz de falsos, i, j, k)}%
\end{footnotesize}
\end{algorithm}

As express�es s�o calculadas de acordo com a tabela verdade de cada operador. Por exemplo, ($exp1$ AND $exp2$) somente retorna verdadeiro para todas as combina��es de verdadeiros nas duas express�es, e falso nos demais casos.

Finalmente, o algoritmo tem complexidade $O(1)$, uma vez que s�o avaliadas posi��es de matrizes que representam a parte esquerda e direita da express�o, as quais s�o passadas por par�metro para a fun��o.


\section{IMPLEMENTA��O}
\label{implementacao}

\subsection{C�digo}

\subsubsection{Arquivos .c}

\begin{itemize}
\item \textbf{main.c:} Arquivo principal respons�vel pela chamada das demais fun��es.
\item \textbf{boolExpression.c:} Implementa as fun��es relacionadas � manipula��o de dados em uma express�o booleana.
\item \textbf{computeExpression.c:} Cont�m as fun��es que determinam o resultado de uma (sub)express�o booleana, para qualquer operador.
\item \textbf{matrix.c:} Possui as fun��es relativas � manipula��o de uma matriz.
\item \textbf{parenthesization.c:} Implementa as combina��es de par�nteses em uma express�o booleana, al�m da contagem do n�mero de express�es que retornam verdadeiro.
\item \textbf{stream.c:} Implementa as fun��es respons�veis pela manipula��o de arquivos.
\end{itemize}

\subsubsection{Arquivos .h}

\begin{itemize}
\item \textbf{boolExpression.h:} Cabe�alho das fun��es relacionadas a uma express�o booleana.
\item \textbf{computeExpression.h:} Define as fun��es relativas �s opera��es booleanas entre duas express�es.
\item \textbf{parenthesization.h:} Arquivo cabe�alho das fun��es relacionadas � determina��o de par�nteses em uma express�o booleana.
\item \textbf{matrix.h:} Cabe�alho das fun��es que tratam da manipula��o de uma matriz.
\item \textbf{stream.h:} Define as fun��es relacionadas � manipula��o de arquivos.
\item \textbf{defineConstants.h:} Cabe�alho que define os valores constantes utilizados no algoritmo.
\end{itemize}

\subsection{Compila��o}

O programa deve ser compilado atrav�s do compilador GCC atrav�s de um makefile a partir do comando $make$.

\subsection{Execu��o}

A execu��o do programa tem como par�metros um arquivo de entrada e outro de sa�da e � realizada a partir do comando \textit{make run} do \textit{makefile} ou pelo seguinte comando:

\begin{footnotesize}
\begin{verbatim} ./tp3 -i <arquivo de entrada> -o <arquivo de sa�da> \end{verbatim}
\end{footnotesize}

\subsubsection{Formato da entrada}

O arquivo de entrada cont�m, na primeira linha, o n�mero de inst�ncias, seguido pelas express�es booleanas nas linhas seguintes:

\begin{footnotesize}
\begin{verbatim}
2
not False or False and True
True and not True and True
\end{verbatim}
\end{footnotesize}

\subsubsection{Formato da sa�da}

A sa�da do programa � apresenta no arquivo de sa�da pelo n�mero de formas poss�veis de defini��es de par�ntesis na express�o booleana, tal que a mesma retorne verdadeiro, sendo uma linha para cada inst�ncia da entrada. O exemplo a seguir � a resposta para a entrada anterior:

\begin{footnotesize}
\begin{verbatim}
5
0
\end{verbatim}
\end{footnotesize}


\section{AVALIA��O EXPERIMENTAL}
\label{avaliacao_experimental}

\subsection{An�lise de desempenho}

Os experimentos a seguir foram executados em um computador pessoal com processador Intel Core i3, 32 bits e 4GB de mem�ria principal, no sistema operacional Ubuntu Linux.

A Tabela~\ref{tempo_execucao_total} mostra o tempo de execu��o total do programa de acordo com o n�mero de termos da express�o booleana:

\begin{table}[ht!]
\centering
\begin{footnotesize}
\begin{tabular}{|c|c|}
\hline
\textbf{\# Termos}	& \textbf{Tempo de execu��o (milisseg.)}\\ \hline
10	& 0,059		\\ \hline
16	& 0,061		\\ \hline
26	& 0,129		\\ \hline
36	& 0,195		\\ \hline
45	& 0,347		\\ \hline
\end{tabular}
\end{footnotesize}
\caption{Tempo de execu��o total do programa. \label{tempo_execucao_total}}
\end{table}

\subsection{Mem�ria alocada}

A Tabela~\ref{memoria_alocada} mostra a quantidade de bytes alocados no programa, de acordo com o n�mero de termos da express�o booleana:

\begin{table}[ht!]
\centering
\begin{footnotesize}
\begin{tabular}{|c|c|}
\hline
\textbf{\# Termos}	& \textbf{Mem�ria alocada (bytes)} \\ \hline
10	& 800		\\ \hline
16	& 2 048		\\ \hline
26	& 5 408		\\ \hline
36	& 10 368	\\ \hline
45	& 16 200	\\ \hline
\end{tabular}
\end{footnotesize}
\caption{Quantidade de bytes alocados. \label{memoria_alocada}}
\end{table}

Note que apenas as matrizes de verdadeiros e falsos dependem do n�mero $t$ de termos, al�m de serem matrizes quadradas do tipo \textit{unsigned long int}. Logo, a quantidade de mem�ria alocada sempre ser� igual a $2\times t�\times 32$, visto que temos duas matrizes $t\times t$ e que um \textit{unsigned long int} tem tamanho 32 bits.

\section{CONSIDERA��ES FINAIS}
\label{conclusao}

O trabalho proposto exp�s uma solu��o satisfat�ria para o problema da contagem de formas de coloca��o de par�nteses em uma express�o booleana com um algoritmo de desempenho polinomial e utilizando uma quantidade de mem�ria razo�vel, visto que, para o m�ximo de termos na express�o, o programa utilizar� 20 kbytes. Contudo, � poss�vel otimizar a utiliza��o de mem�ria, uma vez que existem posi��es das matrizes de verdadeiros e falsos que nunca s�o ocupados.

Al�m do mais, a implementa��o do algoritmo mostrou-se simples para qualquer operador, exceto para o $not$, uma vez que, como operador un�rio, o mesmo possui comportamento diferente dos demais operadores, al�m da possibilidade de existem $n$ operadores $not$ entre duas vari�veis.

\bibliographystyle{sbc}

\section{REFER�NCIAS}

\noindent[1] Cormen, T., Leiserson, C., Rivest, R. and Stein, C. (2009). \textit{Introduction to Algorithms}. MIT Press, 3th edition.

\noindent[2] Ziviani, N. (2007). \textit{Projeto de Algoritmos com implementa��o em Pascal e C}. Pioneira Thomson Learning.

\noindent[3] Vieira, N. (2006). \textit{Introdu��o aos Fundamentos da Computa��o}. Pioneira Thomson Learning.

\noindent[4] cplusplus.com (2000-2012). \textit{http://cplusplus.com/reference/}

\noindent[5] Sedgewick, R. (2001). \textit{Algorithms in C}. Addison-Wesley Professional.

\end{document}
