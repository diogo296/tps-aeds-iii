/*
    - Gerador de testes para ordenação em memória externa -
    Autor: Edson R. de Oliveira Júnior.
    Graduando em ciência da computação pela UFMG.

    Detalhes/Formato de saida exemplo:

    5   //número de contatos (n = 5).
    3   //limitação da memória principal - RAM - (m = 3).
    Amana Piress#87345530       //
    Zwoso Xzeses#33739547       //      Lista contendo os 'n' contatos em ordem
    Pelee Mdacza#74856723         // >    lexográfica aleatória.
    Abcde Salles#94742045        //      Observe que os nomes não contém acentos gráficos.
    Edson Sousaz#89981441        //
                            //<--- observe, aqui, o '\n' no final do arquivo.

*/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int main(){
    unsigned long int contatos, memoria, i;
    unsigned short cFixo1, cFixo2, cFixo3;
    char resposta;
    char nomeArquivo[21];
    FILE *fpo;
    // --- cabeçalho ---
    printf("\n");
    printf(" -----------------------------------------------------\n");
    printf("| Gerador de testes para ordenacao em memoria externa |\n");
    printf(" -----------------------------------------------------\n\n");
    // --- fim ---
    // --- apresentação ---
    printf("Autor: Edson R. de Oliveira Junior.\n");
    printf("Graduando em ciencia da computacao pela UFMG.\n\n");

    printf("Detalhes / Formato de saida-exemplo:\n\n");

    printf("5   //numero de contatos (n = 5).\n");
    printf("3   //limitacao da memoria principal - RAM - (m = 3).\n");
    printf("Amana Piress#87345530   //\n");
    printf("Zwoso Xzeses#33739547   //    Lista contendo os 'n' contatos em ordem\n");
    printf("Pelee Mdacza#74856723   // >  lexografica aleatoria.\n");
    printf("Abcde Salles#94742045   //    Observe que os nomes nao contem acentos graficos.\n");
    printf("Edson Sousaz#89981441   //\n");
    printf("_                       //<--- observe, aqui, o '\\n' no final do arquivo.\n\n");
    // --- fim ---
    do{
        //Define nome do arquivo e o abre em modo escrita.
        printf("\nPor favor digite o nome do arquivo que voce deseja criar (max 20 caracteres):\n");
        scanf("\n%s", nomeArquivo);
        nomeArquivo[20]='\0';
        fpo=fopen(nomeArquivo, "w");
        //Define número de contatos e imprime no arquivo de saida.
        printf("\nPor favor digite o total de contatos que voce deseja gerar:\n");
        scanf("%lu", &contatos);
        fprintf(fpo, "%lu\n", contatos);
        //Define a limitação de memória (tamanho max do buffer) e imprime no arquivo de saida.
        printf("\nQual valor da memoria?\n");
        scanf("%lu", &memoria);
        fprintf(fpo, "%lu\n", memoria);
        //Gera os 'n' contatos aleatóriamente.
        srand(time(NULL));
        for(i=0; i<contatos; i++){
            cFixo1=(rand()%26)+97;
            cFixo2=(rand()%26)+97;
            cFixo3=(rand()%26)+97;
            fprintf(fpo, "%c%c%c%c%c %c%c%c%c%c%c#%u%u\n", (rand()%26)+65, (char)cFixo1, (rand()%26)+97, (char)cFixo2, (char)cFixo3, (rand()%26)+65, (char)cFixo3, (char)cFixo2, (rand()%26)+97, (rand()%26)+97, (char)cFixo1, (rand()%9000)+1000, (rand()%9000)+1000);
        }

        printf("\n\n%c Pronto! Teste gerado com sucesso.\n\n", 16);
        fclose(fpo);

        do{
            printf("\nDeseja criar outro arquivo de teste (s/n)?\n");
            scanf("\n%c", &resposta);
        } while ((resposta != 's') && (resposta != 'n'));
        printf("\n\n\n");
    } while (resposta == 's');

   return 0;
}
