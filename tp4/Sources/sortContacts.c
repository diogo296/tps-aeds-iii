#include "sortContacts.h"

// Divide a entrada em arquivos intermediários.
void splitInput(FILE *input, int numContacts, int bufferSize, int *fileCount) {
	int i;
	_Bool lastFirstContactFlag = false;
	Heap heap;
	heapKey newContact, firstContact;
	FILE *temp;

	createHeap(&heap, bufferSize);
	getContactsBlock(input, 0, &heap, bufferSize);

	temp = openOutputFile(1);

	for (i = bufferSize; i < numContacts; i++) {
		getContactInformation(input, &newContact, 0);
		firstContact = heapExtractMin(&heap);

		// Caso o novo contato seja menor que o primeiro contato, seu flag será o inverso do flag do primeiro contato.
		if (strncmp(newContact.contact, firstContact.contact, CONTACT_MAX_SIZE) < 0) {
			newContact.flag = !firstContact.flag;
		}

		minHeapInsert(&heap, newContact, firstContact.flag);

		if (lastFirstContactFlag != firstContact.flag) {
			changeOutputFile(temp, fileCount);
		}
		writeContact(temp, firstContact.contact);

		lastFirstContactFlag = firstContact.flag;
	}

	while (!emptyHeap(heap.size)) {
		firstContact = heapExtractMin(&heap);

		if (lastFirstContactFlag != firstContact.flag) {
			changeOutputFile(temp, fileCount);
		}
		writeContact(temp, firstContact.contact);

		lastFirstContactFlag = firstContact.flag;
	}

	fclose(temp);
	clearHeap(&heap);
}

// Intercala n arquivos, sendo n menor ou igual ao tamanho do buffer.
void mergeFiles(int *firstFile, int *fileCount, int bufferSize, FILE *output) {
	Heap heap;
	heapKey key;
	FILE **files, *temp;
	int fileID, lastFile = *fileCount;
	int numFiles;

	numFiles = lastFile - *firstFile + 1;

	files = openAllInputFiles(*firstFile, lastFile);

	buildHeap(files, *firstFile, lastFile, &heap, bufferSize);

	*fileCount += 1;

	if (numFiles <= bufferSize) {
		// Arquivo temporário = arquivo final.
		temp = output;
	} else {
		temp = openOutputFile(*fileCount);
	}

	while (!emptyHeap(heap.size)) {
		key = heapExtractMin(&heap);
		writeContact(temp, key.contact);

		fileID = key.fileID;
		getContactInformation(files[fileID], &key, fileID);
		if (!feof(files[fileID])) {
			minHeapInsert(&heap, key, false);
		}
	}

	if (bufferSize < numFiles) {
		fclose(temp);
	}

	clearHeap(&heap);

	*firstFile += bufferSize;

	closeAllInputFiles(files, numFiles);
}

void sortContacts(FILE *input, FILE *output) {
	int numContacts, bufferSize;
	int fileCount = 1, firstFile = 1;

	getNumberInformation(input, &numContacts, &bufferSize);

	splitInput(input, numContacts, bufferSize, &fileCount);

	while (firstFile < fileCount) {
		mergeFiles(&firstFile, &fileCount, bufferSize, output);
	}
}
