#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>

#include "heap.h"

#ifndef STREAM_H_INCLUDED
#define STREAM_H_INCLUDED

#define MAX_FILE_NAME 100

// Abre os arquivos de entrada e saída.
void openIOFiles(FILE **input, FILE **output, int argc, char *argv[]);

// Termina o programa caso um dos arquivos não seja aberto corretamente.
void checkIOFiles(FILE *input, FILE *output);

// Fecha os arquivos de entrada e saída.
void closeIOFiles(FILE *input, FILE *output);

// Abre um arquivo intermediário de saída.
FILE *openOutputFile(int fileID);

// Abre um novo arquivo intermediário de saída.
void changeOutputFile(FILE *output, int *fileCount);

// Abre todos os arquivos intermediários com blocos de contatos ordenados.
FILE **openAllInputFiles(int firstFile, int lastFile);

// Fecha todos os arquivos intermediários de leitura.
void closeAllInputFiles(FILE **files, int numFiles);

// Lê informações do arquivo de entrada referentes a números (número de contatos e tamanho do buffer).
void getNumberInformation(FILE *input, int *numContacts, int *bufferSize);

// Lê um bloco de contatos menor ou igual ao tamanho do buffer de um arquivo.
void getContactsBlock(FILE *input, int fileID, Heap *heap, int bufferSize);

// Constrói o Heap que será utilizado na concatenação de arquivos.
void buildHeap(FILE **files, int firstFile, int lastFile, Heap *heap, int bufferSize);

// Lê as informações contidas no arquivo de entrada referentes a um contato.
void getContactInformation(FILE *input, heapKey *key, int fileID);

// Escreve um contato em um arquivo de saída.
void writeContact(FILE *output, heapKeyType contact);

#endif // STREAM_H_INCLUDED
