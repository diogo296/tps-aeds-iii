#include "stream.h"

void openIOFiles(FILE **input, FILE **output, int argc, char *argv[]) {
	char options[] = "i:o:";
	int option;

	while((option = getopt(argc, argv, options)) != -1) {
		switch(option) {
			case 'i':
		    	*input = fopen(optarg, "r");
	            break;
			case 'o':
				*output = fopen(optarg, "w");
	        	break;
		}
	}
}

void checkIOFiles(FILE *input, FILE *output) {
     if (!input || !output) {
    	 printf("Arquivo nao encontrado!\n");
    	 exit(1);
    }
}

void closeIOFiles(FILE *input, FILE *output) {
	fclose(input);
	fclose(output);
}

FILE *openOutputFile(int fileID) {
	FILE *output;
	char fileName[MAX_FILE_NAME];

	sprintf(fileName, "temp/output_%d.txt", fileID);
	output = fopen(fileName, "w");

	return output;
}

void changeOutputFile(FILE *output, int *fileCount) {
	fclose(output);
	*fileCount += 1;
	output = openOutputFile(*fileCount);
}

FILE **openAllInputFiles(int firstFile, int lastFile) {
	int i;
	FILE **files;
	char fileName[100];
	int numFiles;

	numFiles = (lastFile - firstFile + 1);
	files = (FILE**) malloc(sizeof(FILE*) * (numFiles));

	for (i = 0; i < numFiles; i++) {
		sprintf(fileName, "temp/output_%d.txt", i + firstFile);
		files[i] = fopen(fileName, "r");
	}

	return files;
}

void closeAllInputFiles(FILE **files, int numFiles) {
	int i;

	for (i = 0; i < numFiles; i++) {
		fclose(files[i]);
	}
}

// Lê do arquivo de entrada informações referentes a números.
void getInformation(FILE *input, int *info) {
	fscanf(input, "%d", info);
}

void getNumberInformation(FILE *input, int *numContacts, int *bufferSize) {
	getInformation(input, numContacts);
	getInformation(input, bufferSize);
	fgetc(input);
}

// Lê do arquivo de entrada informações referentes ao contato telefônico.
void getContactInformation(FILE *input, heapKey *key, int fileID) {
	fgets(key->contact, CONTACT_MAX_SIZE, input);
	key->flag = false;
	key->fileID = fileID;
}

void getContactsBlock(FILE *input, int fileID, Heap *heap, int bufferSize) {
	heapKey key;

	while ((heap->size < bufferSize) && !feof(input)) {
		getContactInformation(input, &key, fileID);
		if (!feof(input)) {
			minHeapInsert(heap, key, false);
		}
	}
}

// Insere no heap o primeiro contato de cada arquivo intermediário.
void getFilesFirstContact(FILE **files, int firstFile, int lastFile, Heap *heap, int bufferSize) {
	int i, numFiles;
	heapKey key;

	numFiles = lastFile - firstFile + 1;

	for (i = 0; i < numFiles; i++) {
		if (heap->size < bufferSize) {
			getContactInformation(files[i], &key, i);
			minHeapInsert(heap, key, false);
		}
		else {
			break;
		}
	}
}

void buildHeap(FILE **files, int firstFile, int lastFile, Heap *heap, int bufferSize) {
	int i, numFiles;

	createHeap(heap, bufferSize);

	getFilesFirstContact(files, firstFile, lastFile, heap, bufferSize);

	numFiles = lastFile - firstFile + 1;

	if (heap->size < bufferSize) {
		for (i = 0; i < numFiles; i++) {
			getContactsBlock(files[i], i, heap, bufferSize);
			if (heap->size == bufferSize) {
				break;
			}
		}
	}
}

void writeContact(FILE *output, heapKeyType contact) {
	fprintf(output, "%s", contact);
}
