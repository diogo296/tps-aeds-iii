#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#ifndef HEAP_H_
#define HEAP_H_

#define CONTACT_MAX_SIZE 120

typedef char heapKeyType[CONTACT_MAX_SIZE];

typedef struct {
	heapKeyType contact;
	_Bool flag;
	int fileID;
} heapKey;

typedef struct {
	heapKey *vector;
	int size;
} Heap;

// Cria um heap.
void createHeap(Heap *heap, int heapSize);

// Retorna o menor element odo heap.
heapKey heapMininum(Heap heap);

// Retorna e exclui o menor elemento do heap.
heapKey heapExtractMin(Heap *heap);

// Insere um elemento no heap.
void minHeapInsert(Heap *heap, heapKey key, _Bool lastFirstContactFlag);

// Retorna verdadeiro se o Heap está vazio.
_Bool emptyHeap(int heapSize);

// Libera a memória alocada pelo heap.
void clearHeap(Heap *heap);

#endif /* HEAP_H_ */
