#include "heap.h"

void createHeap(Heap *heap, int heapSize) {
	heap->vector = (heapKey*) malloc(sizeof(heapKey) * heapSize);
	heap->size = 0;
}

// Retorna a posição elemento à esquerda da árvore i.
int left(int i) {
	return (2*i + 1);
}

// Retorna a posição elemento à direita da árvore i.
int right(int i) {
	return (2*i + 2);
}

// Retorna a posição elemento à direita da árvore i.
int parent(int i) {
	return ((i-1)/2);
}

// Troca o valor de dois elementos.
void swap(heapKey *heapVector, int i, int j) {
	heapKey aux;
	aux = heapVector[i];
	heapVector[i] = heapVector[j];
	heapVector[j] = aux;
}

_Bool isLower(heapKey key1, heapKey key2, _Bool lastFirstContactFlag) {
	if (key1.flag == key2.flag) {
		return (strncmp(key1.contact, key2.contact, CONTACT_MAX_SIZE) < 0);
	}
	else {
		return (key2.flag != lastFirstContactFlag);
	}
}

_Bool isHigher(heapKey key1, heapKey key2, _Bool lastFirstContactFlag) {
	if (key1.flag == key2.flag) {
		return (strncmp(key1.contact, key2.contact, CONTACT_MAX_SIZE) > 0);
	}
	else {
		return (key1.flag != lastFirstContactFlag);
	}
}

void minHeap(Heap *heap, int i, _Bool lastFirstContactFlag) {
	int head;
	int l = left(i);
	int r = right(i);

	if ((l < heap->size) && isLower(heap->vector[l], heap->vector[i], lastFirstContactFlag)) {
		head = l;
	}
	else {
		head = i;
	}

	if ((head < heap->size) && (r < heap->size) && isLower(heap->vector[r], heap->vector[head], lastFirstContactFlag)) {
		head = r;
	}

	if (head != i) {
		swap(heap->vector, i, head);
		minHeap(heap, head, lastFirstContactFlag);
	}
}

heapKey heapExtractMin(Heap *heap) {
	heapKey minimum = heap->vector[0];
	heap->vector[0] = heap->vector[heap->size-1];
	heap->size--;
	minHeap(heap, 0, minimum.flag);

	return minimum;
}

void heapIncreaseKey(Heap *heap, int i, heapKey key, _Bool lastFirstContactFlag) {
	heap->vector[i] = key;

	while ((i >= 1) && (isHigher(heap->vector[parent(i)], heap->vector[i], lastFirstContactFlag))) {
		swap(heap->vector, parent(i), i);
		i = parent(i);
	}
}

void minHeapInsert(Heap *heap, heapKey key, _Bool lastFirstContactFlag) {
	heap->size++;
	heapIncreaseKey(heap, heap->size-1, key, lastFirstContactFlag);
}

_Bool emptyHeap(int heapSize) {
	return (heapSize == 0);
}

void clearHeap(Heap *heap) {
	free(heap->vector);
	heap->size = 0;
}
