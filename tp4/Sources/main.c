#include <stdlib.h>
#include <stdio.h>

#include "sortContacts.h"

int main(int argc, char *argv[]) {
	FILE *input, *output;

	openIOFiles(&input, &output, argc, argv);
	checkIOFiles(input, output);

	sortContacts(input, output);

	closeIOFiles(input, output);

	return 0;
}
