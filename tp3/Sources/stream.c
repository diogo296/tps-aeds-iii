#include "stream.h"

void checkFiles(FILE *input, FILE *output) {
     if (!input || !output) {
    	 printf("Arquivo nao encontrado!\n");
    	 exit(1);
    }
}

void closeFiles(FILE *input, FILE *output) {
	fclose(input);
	fclose(output);
}

int getInstances(FILE *input) {
	int instances;
	fscanf(input, "%d", &instances);
	return instances;
}

int getInformation(char *word) {
    int information;

    switch(word[0]) {
    	case 'F':
    		information = false;
    		break;
    	case 'T':
    		information = true;
    		break;
    	case 'a':
    		information = AND;
    		break;
    	case 'o':
    		information = OR;
    		break;
    	case 'x':
    		information = XOR;
    		break;
    	case 'n':
    		if (word[1] == 'a') {
    			information = NAND;
    		}
    		else {
    			information = NOT;
    		}
    		break;
    	case 'i':
    		information = IMP;
    		break;
    	case 'e':
    		information = EQV;
    		break;
    }

    return information;
}

void getBoolExpression(FILE *input, int expression[], int *expSize) {
	int i = 0;
	char word[6];

	do {
		fscanf(input, "%s", word);
		expression[i] = getInformation(word);
		i++;
	} while (fgetc(input) == ' ');

	*expSize = i;
}

void writeNumParenthesizations(FILE *output, matrixType numParenthesizatins, int numInstances) {
	fprintf(output, "%lu", numParenthesizatins);

	// Para não imprimir nova linha após a última instância.
    if(numInstances != 1) {
        fprintf(output, "\r\n");
    }
}
