#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "defineConstants.h"
#include "matrix.h"

#ifndef STREAM_H_INCLUDED
#define STREAM_H_INCLUDED

// Termina o programa caso um dos arquivos não seja aberto corretamente.
void checkFiles(FILE *input, FILE *output);

// Fecha os arquivos de entrada e saída.
void closeFiles(FILE *input, FILE *output);

// Retorna o número de instâncias lida do arquivo.
int getInstances(FILE *input);

// Retorna uma informação lida do arquivo.
int getInformation(char *word);

// Le uma expressão booleana do arquivo de entrada.
void getBoolExpression(FILE *input, int expression[], int *expSize);

// Escreve a quantidade de parêntesis possíveis no arquivo de saída.
void writeNumParenthesizations(FILE *output, matrixType numParenthesizatins, int numInstances);

#endif // STREAM_H_INCLUDED
