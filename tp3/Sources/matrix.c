#include "matrix.h"

void printMatrix(matrixType **matrix, int size) {
	int i, j;

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			printf("%lu ", matrix[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

matrixType **createMatrix(int size) {
	int i, j;
	matrixType **matrix;

	matrix = (matrixType**) malloc(sizeof(matrixType*) * size);

	for (i = 0; i < size; i++) {
		matrix[i] = (matrixType*) malloc(sizeof(matrixType) * size);
	}

	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			matrix[i][j] = 0;
		}
	}

	return matrix;
}

_Bool isNotSet(matrixType **truesMatrix, matrixType **falsesMatrix, int i, int j) {
	return ((truesMatrix[i][j] == 0) && (falsesMatrix[i][j] == 0));
}

// Libera a memória alocada por uma matriz.
void freeMatrix(matrixType **matrix, int matrixSize) {
	int i;

	for (i = 0; i < matrixSize; i++) {
		free(matrix[i]);
	}

	free(matrix);
}

void freeAllAlocatedMemory(matrixType **truesMatrix, matrixType **falsesMatrix, int expSize) {
	freeMatrix(truesMatrix, expSize);
	freeMatrix(falsesMatrix, expSize);
}
