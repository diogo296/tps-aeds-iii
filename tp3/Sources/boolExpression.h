#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "defineConstants.h"

#ifndef BOOLEXPRESSION_H_
#define BOOLEXPRESSION_H_

// Retorna verdadeiro se o valor da expressão é uma variável.
_Bool isVariable(int value);

// Retorna verdadeiro se o valor da expressão é um operador.
_Bool isOperator(int value);

// Imprime a expressão booleana na tela
void printExpression(int expression[], int expSize);

#endif /* BOOLEXPRESSION_H_ */
