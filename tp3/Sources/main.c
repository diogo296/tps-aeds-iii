#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>

#include "stream.h"
#include "parenthesization.h"

//double time() {
//	struct timeval tv;
//	gettimeofday(&tv,0);
//	return tv.tv_sec + tv.tv_usec/1e6;
//}

int main(int argc, char *argv[]) {
//	double beginTime, endTime;
//	beginTime = time();

    int numInstances, expSize;
    int expression[EXP_MAX_SIZE];
    matrixType numParenthesizations;
    FILE *input, *output;

    input  = fopen(argv[2], "r");
    output = fopen(argv[4], "w");
    checkFiles(input, output);

    numInstances = getInstances(input);

    while (numInstances > 0) {
    	getBoolExpression(input, expression, &expSize);
    	printf("Expression size: %d\n", expSize);

    	numParenthesizations = parenthesization(expression, expSize);

    	writeNumParenthesizations(output, numParenthesizations, numInstances);

        numInstances--;

//        endTime = time();
//        printf ("Time: %lfs\n\n", endTime - beginTime);
//        beginTime = endTime;
    }

    closeFiles(input, output);

    return 0;
}
