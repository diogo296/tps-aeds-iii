#include <stdio.h>

#include "defineConstants.h"
#include "matrix.h"

#ifndef COMPUTEEXPRESSION_H_
#define COMPUTEEXPRESSION_H_

// Calcula o número de verdadeiros e falsos entre duas (sub)expressões, de acordo com o operador.
void computeExpression(int operator, matrixType **truesMatrix, matrixType **falsesMatrix, int i, int j, int k);

#endif /* COMPUTEEXPRESSION_H_ */
