#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "defineConstants.h"
#include "boolExpression.h"
#include "computeExpression.h"

#ifndef PARENTHESIZATION_H_
#define PARENTHESIZATION_H_

// Retorna o número de combinações possíveis de parêntesis em uma expressão booleana, tal que a mesma retorne true.
matrixType parenthesization(int expression[], int expSize);

#endif /* PARENTHESIZATION_H_ */
