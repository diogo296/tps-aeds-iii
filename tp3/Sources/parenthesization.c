#include "parenthesization.h"

// Atrubui valores à matriz de verdadeiros e falsos de acordo com o valor da variável.
void setWithVariableValue(matrixType **truesMatrix, matrixType **falsesMatrix, int i, int variable) {
	if (variable == true) {
		truesMatrix[i][i] = 1;
	}
	else {
		falsesMatrix[i][i] = 1;
	}
}

// Função recursiva auxiliar da função parenthesization.
void computeParenthesizations(int i, int j, matrixType **truesMatrix, matrixType **falsesMatrix, int expression[]) {
	int k;

	// Caso base: parêntesis esquerdo e direito na variável i da expressão.
	if (i == j) {
		setWithVariableValue(truesMatrix, falsesMatrix, i, expression[i]);
	}

	else {
		// Para cada operador da (sub)expressão:
		for (k = i; k < j; k++) {
			if (isOperator(expression[k])) {
				if (expression[k] != NOT) {
					if (isNotSet(truesMatrix, falsesMatrix, i, k-1)) {
						// Calcula recursivamente a parte esquerda da (sub)expressão.
						computeParenthesizations(i, k-1, truesMatrix, falsesMatrix, expression);
					}
				}
				if (isNotSet(truesMatrix, falsesMatrix, k+1, j)) {
					// Calcula recursivamente a parte direita da (sub)expressão.
					computeParenthesizations(k+1, j, truesMatrix, falsesMatrix, expression);
				}
				// Calcula o número de trues e falses entre as subexpressões [i][k-1] e [k+1][j], de acordo com o operador.
				computeExpression(expression[k], truesMatrix, falsesMatrix, i, j, k);
			}
		}
	}
}

matrixType parenthesization(int expression[], int expSize) {
	matrixType **truesMatrix, **falsesMatrix;
	matrixType numParenthesizations;

	falsesMatrix = createMatrix(expSize);
	truesMatrix = createMatrix(expSize);

	computeParenthesizations(0, expSize-1, truesMatrix, falsesMatrix, expression);

	numParenthesizations = truesMatrix[0][expSize-1];

//	printf("Bytes: %d\n\n", (sizeof(truesMatrix) * expSize * expSize * 2));

	freeAllAlocatedMemory(truesMatrix, falsesMatrix, expSize);

	return numParenthesizations;
}
