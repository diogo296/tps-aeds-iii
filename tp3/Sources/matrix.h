#include <stdlib.h>
#include <stdio.h>

#ifndef MATRIX_H_
#define MATRIX_H_

typedef unsigned long int matrixType;

// Imprime a matriz na tela.
void printMatrix(matrixType **matrix, int size);

// Aloca uma matriz do tamanho da expressão booleana com todos os elementos iguais a zero.
matrixType **createMatrix(int expSize);

// Retorna verdadeiro se ainda não foi determinado o valor das matrizes de verdadeiros e falsos na posição [i][j].
_Bool isNotSet(matrixType **truesMatrix, matrixType **falsesMatrix, int i, int j);

// Libera toda a memória alocada pelas matrizes de verdadeiros e falsos.
void freeAllAlocatedMemory(matrixType **truesMatrix, matrixType **falsesMatrix, int expSize);

#endif /* MATRIX_H_ */
