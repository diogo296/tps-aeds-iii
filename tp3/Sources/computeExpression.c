#include "computeExpression.h"

// Calcula o número de verdadeiros e falsos entre duas (sub)expressões para o operador AND (conjunção).
void computeAND(matrixType **truesMatrix, matrixType **falsesMatrix, int i, int j, int k) {
	matrixType left = truesMatrix[i][k-1] + falsesMatrix[i][k-1];        // T(esq) + F(esq)
	matrixType right = truesMatrix[k+1][j] + falsesMatrix[k+1][j];       // T(dir) + F(dir)
	matrixType totalTrues = truesMatrix[i][k-1] * truesMatrix[k+1][j];   // T(esq) * T(dir)

	truesMatrix[i][j] += totalTrues;
	falsesMatrix[i][j] += (left * right) - totalTrues;
}

// Calcula o número de verdadeiros e falsos entre duas (sub)expressões para o operador OR (disjunção inclusiva).
void computeOR(matrixType **truesMatrix, matrixType **falsesMatrix, int i, int j, int k) {
	matrixType left = truesMatrix[i][k-1] + falsesMatrix[i][k-1];			// T(esq) + F(esq)
	matrixType right = truesMatrix[k+1][j] + falsesMatrix[k+1][j];		    // T(dir) + F(dir)
	matrixType totalFalses = falsesMatrix[i][k-1] * falsesMatrix[k+1][j];   // F(esq) * F(dir)

	truesMatrix[i][j] += (left * right) - totalFalses;
	falsesMatrix[i][j] += totalFalses;
}

// Calcula o número de verdadeiros e falsos entre duas (sub)expressões para o operador XOR (disjunção exclusiva).
void computeXOR(matrixType **truesMatrix, matrixType **falsesMatrix, int i, int j, int k) {
	matrixType trues1 = truesMatrix[i][k-1] * falsesMatrix[k+1][j];	    // T(esq) * F(dir)
	matrixType trues2 = falsesMatrix[i][k-1] * truesMatrix[k+1][j];	    // F(esq) * T(dir)
	matrixType falses1 = truesMatrix[i][k-1] * truesMatrix[k+1][j];	    // T(esq) * T(dir)
	matrixType falses2 = falsesMatrix[i][k-1] * falsesMatrix[k+1][j];   // F(esq) * F(dir)

	truesMatrix[i][j] += trues1 + trues2;
	falsesMatrix[i][j] += falses1 + falses2;
}

// Calcula o número de verdadeiros e falsos entre duas (sub)expressões para o operador NOT (negação).
void computeNOT(matrixType **truesMatrix, matrixType **falsesMatrix, int i, int j, int k) {
	truesMatrix[i][j] += falsesMatrix[k+1][j];   // F(dir)
	falsesMatrix[i][j] += truesMatrix[k+1][j];   // T(dir)
}

// Calcula o número de verdadeiros e falsos entre duas (sub)expressões para o operador NAND (negação da conjunção).
void computeNAND(matrixType **truesMatrix, matrixType **falsesMatrix, int i, int j, int k) {
	matrixType left = truesMatrix[i][k-1] + falsesMatrix[i][k-1];		  // T(esq) + F(esq)
	matrixType right = truesMatrix[k+1][j] + falsesMatrix[k+1][j];		  // T(dir) + F(dir)
	matrixType totalFalses = truesMatrix[i][k-1] * truesMatrix[k+1][j];   // T(esq) * T(dir)

	truesMatrix[i][j] += (left * right) - totalFalses;
	falsesMatrix[i][j] += totalFalses;
}

// Calcula o número de verdadeiros e falsos entre duas (sub)expressões para o operador IMP (implicação).
void computeIMP(matrixType **truesMatrix, matrixType **falsesMatrix, int i, int j, int k) {
	matrixType trues1 = truesMatrix[i][k-1] * truesMatrix[k+1][j];	   // T(esq) * T(dir)
	matrixType trues2 = falsesMatrix[i][k-1] * truesMatrix[k+1][j];	   // F(esq) * T(dir)
	matrixType trues3 = falsesMatrix[i][k-1] * falsesMatrix[k+1][j];   // F(esq) * F(dir)
	matrixType falses = truesMatrix[i][k-1] * falsesMatrix[k+1][j];    // T(esq) * F(dir)

	truesMatrix[i][j] += trues1 + trues2 + trues3;
	falsesMatrix[i][j] += falses;
}

// Calcula o número de verdadeiros e falsos entre duas (sub)expressões para o operador EQV (equivalência).
void computeEQV(matrixType **truesMatrix, matrixType **falsesMatrix, int i, int j, int k) {
	matrixType trues1 = truesMatrix[i][k-1] * truesMatrix[k+1][j];	   // T(esq) * T(dir)
	matrixType trues2 = falsesMatrix[i][k-1] * falsesMatrix[k+1][j];   // F(esq) * F(dir)
	matrixType falses1 = truesMatrix[i][k-1] * falsesMatrix[k+1][j];   // T(esq) * F(dir)
	matrixType falses2 = falsesMatrix[i][k-1] * truesMatrix[k+1][j];   // F(esq) * T(dir)

	truesMatrix[i][j] += trues1 + trues2;
	falsesMatrix[i][j] += falses1 + falses2;
}

void computeExpression(int operator, matrixType **truesMatrix, matrixType **falsesMatrix, int i, int j, int k) {
	switch(operator) {
		case AND:
			computeAND(truesMatrix, falsesMatrix, i, j, k);
			break;
		case OR:
			computeOR(truesMatrix, falsesMatrix, i, j, k);
			break;
		case XOR:
			computeXOR(truesMatrix, falsesMatrix, i, j, k);
			break;
		case NAND:
			computeNAND(truesMatrix, falsesMatrix, i, j, k);
			break;
		case NOT:
			if (isNotSet(truesMatrix, falsesMatrix, i, j)) {
				computeNOT(truesMatrix, falsesMatrix, i, j, k);
			}
			break;
		case IMP:
			computeIMP(truesMatrix, falsesMatrix, i, j, k);
			break;
		case EQV:
			computeEQV(truesMatrix, falsesMatrix, i, j, k);
			break;
	}
}
