#ifndef DEFINESTRINGS_H_
#define DEFINESTRINGS_H_

// Número máximo de termos de uma expressão.
#define EXP_MAX_SIZE 50

enum operators{ AND = 2, OR, XOR, NAND, NOT, IMP, EQV };

#endif /* DEFINESTRINGS_H_ */
