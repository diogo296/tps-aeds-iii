#include "boolExpression.h"

_Bool isVariable(int value) {
	return ((value == false) || (value == true));
}

_Bool isOperator(int value) {
	return (value >= 2);
}

// Imprime um elemento de uma expressão.
void printValue(int value) {
	switch (value) {
		case false:
			printf("False ");
			break;
		case true:
			printf("True ");
			break;
		case AND:
			printf("and ");
			break;
		case OR:
			printf("or ");
			break;
		case XOR:
			printf("xor ");
			break;
		case NAND:
			printf("nand ");
			break;
		case NOT:
			printf("not ");
			break;
		case IMP:
			printf("imp ");
			break;
		case EQV:
			printf("eqv ");
			break;
	}
}

void printExpression(int expression[], int expSize) {
	int i;
	for (i = 0; i < expSize; i++) {
		printValue(expression[i]);
	}
	printf("\n");
}
