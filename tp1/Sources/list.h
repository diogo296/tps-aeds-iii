#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED

#include "node.h"
#include <stdio.h>

#define FALSE 0
#define TRUE 1

typedef struct {
    node *end;  // N� marcador do fim da lista.
    int size;   // N�mero de elementos da lista.
    int info;   // Cont�m a nota do aluno ou o n�mero de vagas da universidade.
} List;

// Inicializa a lista.
void ListConstructor(List *list);

// Retorna um ponteiro para o primeiro node da lista em O(1).
node* begin(node* listEnd);

// Retorna o valor da chave do primeiro elemento da lista em O(1).
type front(node* listEnd);

// Retorna o n� que cont�m a chave x em O(n).
node* find(type x, node* listEnd);

// Remove um n� da lista em O(1);
void erase(node *n, List *list);

// Remove o primeiro elemento da lista em O(1).
void popFront(List *list);

// Insere x no final da lista em O(1).
void pushBack(type x, List *list);

// Insere x no in�cio da lista em O(1).
void pushFront(type x, List *list);

// Retorna TRUE caso a lista seja vazia e FALSE caso contr�rio.
int empty(int listSize);

// Remove todos os elementos da lista em O(n) e libera a mem�ria alocada,
// onde n � o n�mero de elementos na lista.
void clear(List *list);

#endif // LIST_H_INCLUDED
