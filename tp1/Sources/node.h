#ifndef NODE_H_INCLUDED
#define NODE_H_INCLUDED

#include <stdlib.h>

typedef int type;

typedef struct Node {
    type key;           // Conte�do do n�.
    int weight;         // Peso do n�: indica o grau de prefer�ncia de um aluno.
    struct Node *next;  // Apontador para o pr�ximo n�.
    struct Node *prev;  // Apotador para o n� anterior.
} node;

// Cria o n� sentinela.
node* NewSentinel();

// Cria um novo node.
node* NewNode(type k, node *left, node *right);

#endif // NODE_H_INCLUDED
