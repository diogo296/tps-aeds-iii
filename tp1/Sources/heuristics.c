#include "heuristics.h"

void approve(List *studentsQueue, int univID, int prefWeight, List* studentsTable, List* univTable) {
    int studentID = front(studentsQueue->end);
    pushBack(studentID, &univTable[univID-1]);
    univTable[univID-1].end->prev->weight = prefWeight;
    popFront(studentsQueue);
}

void putOffStudent(List *studentsQueue, List *studentList, List *univList, node *student) {
    pushFront(student->key, studentsQueue);
    erase(student, univList);
}

int thereAreVacancies(List univList) {
    return !(univList.info == univList.size);
}

int allVacanciesFilled(List *univTable, int numUniv) {
    int i;
    for (i = 0; i < numUniv; i++) {
        if (thereAreVacancies(univTable[i])) {
            return FALSE;
        }
    }
    return TRUE;
}

int isLastPreference(List studentList, int studentPref) {
    return (studentList.end->prev->key == studentPref);
}

int isVacancyPriority(int studentID1, int studentID2, int univID, List *studentsTable) {
    if (isLastPreference(studentsTable[studentID1-1], univID) &&
        !isLastPreference(studentsTable[studentID2-1], univID)) {
        return TRUE;
    }
    else return FALSE;
}

int isPreferencePriority(int studentID, int prefWeight1, node *student2, List *studentsTable) {
    int prefWeight2 = student2->weight;
    int studentGrade1 = studentsTable[studentID-1].info;
    int studentGrade2 = studentsTable[student2->key-1].info;

    if ( (prefWeight1 < prefWeight2) || ((prefWeight1 == prefWeight2) && (studentGrade1 > studentGrade2)) ) {
        return TRUE;
    }

    else return FALSE;
}

int isGradePriority(int studentID, int prefWeight1, node *student2, List *studentsTable) {
    int prefWeight2 = student2->weight;
    int studentGrade1 = studentsTable[studentID-1].info;
    int studentGrade2 = studentsTable[student2->key-1].info;

    if ( (studentGrade1 > studentGrade2) || ((studentGrade1 == studentGrade2) && (prefWeight1 < prefWeight2)) ) {
        return TRUE;
    }

    else return FALSE;
}

node *prefersOtherStudent_studHeur(int studentID, int univID, int prefWeight, List *studentsTable,
                                   List *univTable, int numUniv) {
    node *n;
    node *lowerPref = univTable[univID-1].end;
    int allVacFilledFlag = allVacanciesFilled(univTable, numUniv);

    if (!allVacFilledFlag) {
        for (n = begin(univTable[univID-1].end); n != univTable[univID-1].end; n = n->next) {
            if ( isVacancyPriority(studentID, n->key, univID, studentsTable) ||
                (!isLastPreference(studentsTable[n->key-1], univID) &&
                 isPreferencePriority(studentID, prefWeight, n, studentsTable)) ) {
                lowerPref = n;
            }
        }
    }

    else {
        for (n = begin(univTable[univID-1].end); n != univTable[univID-1].end; n = n->next) {
            if (isPreferencePriority(studentID, prefWeight, n, studentsTable)) {
                lowerPref = n;
            }
        }
    }

    return lowerPref;
}

node *prefersOtherStudent_univHeur(int studentID, int univID, int prefWeight, List *studentsTable,
                                   List *univTable, int numUniv) {
    node *n;
    node *lowerPref = univTable[univID-1].end;
    int allVacFilledFlag = allVacanciesFilled(univTable, numUniv);

    if (!allVacFilledFlag) {
        for (n = begin(univTable[univID-1].end); n != univTable[univID-1].end; n = n->next) {
            if ( isVacancyPriority(studentID, n->key, univID, studentsTable) ||
                (!isLastPreference(studentsTable[n->key-1], univID) &&
                 isGradePriority(studentID, prefWeight, n, studentsTable)) ) {
                lowerPref = n;
            }
        }
    }

    else {
        for (n = begin(univTable[univID-1].end); n != univTable[univID-1].end; n = n->next) {
            if (isGradePriority(studentID, prefWeight, n, studentsTable)) {
                lowerPref = n;
            }
        }
    }

    return lowerPref;
}

void studentsQueueConstructor(List* studentsQueue, int numStudents) {
    ListConstructor(studentsQueue);
    int i;
    for (i = 1; i <= numStudents; i++) {
        pushBack(i, studentsQueue);
    }
}

void studentHeuristic(List* studentsTable, List* univTable, int numStudents, int numUniv) {
    int studentID, univID;
    node *preference = begin(studentsTable[0].end);
    node *prefOtherStud;
    // Fila de alunos ainda n�o aprovados.
    List studentsQueue;
    studentsQueueConstructor(&studentsQueue, numStudents);

    while (!empty(studentsQueue.size)) {
        studentID = front(studentsQueue.end);
        univID = preference->key;
        prefOtherStud = prefersOtherStudent_studHeur(studentID, univID, preference->weight, studentsTable, univTable, numUniv);

        // H� vagas na universidade.
        if (thereAreVacancies(univTable[univID-1])) {
            approve(&studentsQueue, univID, preference->weight, studentsTable, univTable);
            if (!empty(studentsQueue.size)) {
                studentID = front(studentsQueue.end);
                preference = begin(studentsTable[studentID-1].end);
            }
        }

        // N�o h� vagas na universidade; substitui um aluno pelo estudante de studentID se for conveniente.
        else if (prefOtherStud != univTable[univID-1].end) {
            approve(&studentsQueue, univID, preference->weight, studentsTable, univTable);
            studentID = prefOtherStud->key;
            putOffStudent(&studentsQueue, &studentsTable[studentID-1], &univTable[univID-1], prefOtherStud);
            if (!empty(studentsQueue.size)) {
                studentID = front(studentsQueue.end);
                preference = begin(studentsTable[studentID-1].end);
            }
        }

        // Estudante reprovado pela universidade.
        else {
            preference = preference->next;
            if (preference == studentsTable[studentID-1].end) {
                // Estudante reprovado por todas as universidades.
                popFront(&studentsQueue);
                if (!empty(studentsQueue.size)) {
                    studentID = front(studentsQueue.end);
                    preference = begin(studentsTable[studentID-1].end);
                }
            }
        }
    }
}

void univHeuristic(List* studentsTable, List* univTable, int numStudents, int numUniv) {
    int studentID, univID;
    node *preference = begin(studentsTable[0].end);
    node *prefOtherStud;
    // Fila de alunos ainda n�o aprovados.
    List studentsQueue;
    studentsQueueConstructor(&studentsQueue, numStudents);

    while (!empty(studentsQueue.size)) {
        studentID = front(studentsQueue.end);
        univID = preference->key;
        prefOtherStud = prefersOtherStudent_univHeur(studentID, univID, preference->weight, studentsTable, univTable, numUniv);

        // H� vagas na universidade.
        if (thereAreVacancies(univTable[univID-1])) {
            approve(&studentsQueue, univID, preference->weight, studentsTable, univTable);
            if (!empty(studentsQueue.size)) {
                studentID = front(studentsQueue.end);
                preference = begin(studentsTable[studentID-1].end);
            }
        }

        // N�o h� vagas na universidade; substitui um aluno pelo estudante de studentID se for conveniente.
        else if (prefOtherStud != univTable[univID-1].end) {
            approve(&studentsQueue, univID, preference->weight, studentsTable, univTable);
            studentID = prefOtherStud->key;
            putOffStudent(&studentsQueue, &studentsTable[studentID-1], &univTable[univID-1], prefOtherStud);
            if (!empty(studentsQueue.size)) {
                studentID = front(studentsQueue.end);
                preference = begin(studentsTable[studentID-1].end);
            }
        }

        // Estudante reprovado pela universidade.
        else {
            preference = preference->next;
            if (preference == studentsTable[studentID-1].end) {
                // Estudante reprovado por todas as universidades.
                popFront(&studentsQueue);
                if (!empty(studentsQueue.size)) {
                    studentID = front(studentsQueue.end);
                    preference = begin(studentsTable[studentID-1].end);
                }
            }
        }
    }
}
