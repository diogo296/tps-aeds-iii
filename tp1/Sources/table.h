#ifndef TABLE_H_INCLUDED
#define TABLE_H_INCLUDED

#include "list.h"

// Inicializa o conjunto.
List *TableConstructor(int tableSize);

// Remove todos os elementos do conjunto em O(n�).
void resetTable(List *table, int tableSize);

// Remove todos os elementos do conjunto em O(n�) e libera o espa�o alocado pelo vetor.
void clearTable(List *table, int tableSize);

#endif // TABLE_H_INCLUDED
