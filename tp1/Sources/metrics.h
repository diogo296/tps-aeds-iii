#ifndef METRICS_H_INCLUDED
#define METRICS_H_INCLUDED

#include "list.h"
#include "createVector.h"

// Calcula a media do percentual de vagas preenchidas de das universidades.
float averagePercVacancies(List *univTable, int numUniv);

// Calcula a mediana do percentual d vagas preenchidas de todas as universidades.
float medianPercVacancies(List *univTable, int numUniv);

// Calcula a media da satisfação dos estudantes.
float averageStudentsSatisf(List *univTable, int numUniv);

// Calcula a media da satisfação dos estudantes.
float medianStudentsSatisf(List *univTable, int numUniv);

// Calcula a media da satisfação das universidades.
float averageUnivSatisf(List *univTable, List *studentsTable, int numUniv);

// Calcula a mediana da satisfação das universidades.
float medianUnivSatisf(List *univTable, List *studentsTable, int numUniv);

// Calcula a media das notas de corte das universidades
float averageCutoff(List *univTable, List* studentsTable, int numUniv);

// Calcula a mediana das notas de corte das universidades
float medianCutoff(List *univTable, List* studentsTable, int numUniv);

#endif // METRICS_H_INCLUDED
