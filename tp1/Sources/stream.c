#include "stream.h"

void checkFiles(FILE* input, FILE* output) {
	if (!input || !output) {
		printf("Abortado\n");
		abort();
	}
}

type getInformation(FILE* input) {
    type information;
    fscanf(input, "%d", &information);
    return information;
}

void getGeneralInfo(FILE *input, int *numStudents, int *numUniv, int *maxPrefListSize) {
	*numStudents = getInformation(input);
	*numUniv = getInformation(input);
	*maxPrefListSize = getInformation(input);
}

void getStudentsInfo(FILE *input, List *table, int numSubjects, int maxPrefListSize) {
    int i, weight;
    type preference;

    for (i = 0; i < numSubjects; i++) {
    	table[i].info = getInformation(input);
        printf("[%d]", table[i].info);
        while (fgetc(input) == ' ') {
        	preference = getInformation(input);
            pushBack(preference, &table[i]);
            table[i].end->prev->weight = weight;
            weight++;
            printf(" %d", table[i].end->prev->key);
        }
        printf("\n");
    }
}

void getUnivInfo(FILE *input, List *table, int numUniv) {
    int i;
    for (i = 0; i < numUniv; i++) {
        table[i].info = getInformation(input);
    }
}

void writeApproveds(FILE *output, List *table, int tableSize) {
    int i;
    node *n;
    for (i = 0; i < tableSize; i++) {
        fprintf(output, "%d:", i+1);
        for (n = begin(table[i].end); n != table[i].end; n = n->next) {
            fprintf(output, " %d", n->key);
        }
        fprintf(output, "\r\n");
    }
}

void writePercVacanciesMetrics(FILE *output, List *univList, int numUniv) {
    char percent = '%';
    fprintf(output, "%.0f%c ", averagePercVacancies(univList, numUniv), percent);
    fprintf(output, "%.0f%c\r\n", medianPercVacancies(univList, numUniv), percent);
}

void writeStudentsSatisfMetrics(FILE *output, List *univList, int numUniv) {
    fprintf(output, "%.3f ", averageStudentsSatisf(univList, numUniv));
    fprintf(output, "%.3f\r\n", medianStudentsSatisf(univList, numUniv));
}

void writeUnivSatisfMetrics(FILE *output, List *univList, List *studentsTable, int numUniv) {
    fprintf(output, "%.3f ", averageUnivSatisf(univList, studentsTable, numUniv));
    fprintf(output, "%.3f\r\n", medianUnivSatisf(univList, studentsTable, numUniv));
}

void writeCutoffMetrics(FILE *output, List *univList, List *studentsTable, int numUniv) {
    fprintf(output, "%.3f ", averageCutoff(univList, studentsTable, numUniv));
    fprintf(output, "%.3f", medianCutoff(univList, studentsTable, numUniv));
}

void writeSatisfactionMetrics(FILE *output, List *univTable, List *studentsTable, int numUniv) {
    writePercVacanciesMetrics(output, univTable, numUniv);
    writeStudentsSatisfMetrics(output, univTable, numUniv);
    writeUnivSatisfMetrics(output, univTable, studentsTable, numUniv);
    writeCutoffMetrics(output, univTable, studentsTable, numUniv);
}
