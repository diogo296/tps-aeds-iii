#ifndef STREAM_H_INCLUDED
#define STREAM_H_INCLUDED

#include <stdio.h>
#include <string.h>

#include "list.h"
#include "metrics.h"

// Checa se os arquivos foram abertos corretamente.
void checkFiles(FILE* input, FILE* output);

// Retorna uma informa��o lida do arquivo.
type getInformation(FILE *input);

void getGeneralInfo(FILE *input, int *numStudents, int *numUniv, int *maxPrefListSize);

// Guarda as informa��es dos alunos.
void getStudentsInfo(FILE *input, List *studentsTable, int numStudents, int maxPrefListSize);

// Guarda as informa��es das universidades.
void getUnivInfo(FILE *input, List *univTable, int numUniv);

// Escreve todos os aprovados em todas as universidades em um arquivo.
void writeApproveds(FILE *output, List *univTable, int tableSize);

// Escreve as m�tricas de percentual de vagas ocupadas em um arquivo.
void writePercVacanciesMetrics(FILE *output, List *univTable, int numUniv);

// Escreve as m�tricas de satisfa��o dos estudantes em um arquivo.
void writeStudentsSatisfMetrics(FILE *output, List *univTable, int numUniv);

// Escreve as m�tricas de satisfa��o das universidades em um arquivo.
void writeUnivSatisfMetrics(FILE *output, List *univTable, List *studentsTable, int numUniv);

// Escreve as m�tricas das notas de corte das universidades em um arquivo.
void writeCutoffMetrics(FILE *output, List *univTable, List *studentsTable, int numUniv);

// Escreve as m�tricas de c�lculo de satisfa��o dos alunos e das universidades em um arquivo.
void writeSatisfactionMetrics(FILE *output, List *univTable, List *studentsTable, int numUniv);

#endif // STREAM_H_INCLUDED
