#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "table.h"
#include "heuristics.h"
#include "stream.h"

int main(int argc, char *argv[])
{
    List *studentsTable, *univTable;
    type numInstances, numStudents, numUniversities, maxPrefListSize;

    FILE *input = fopen(argv[2], "r");
    FILE *output = fopen(argv[4], "w");
    checkFiles(input, output);

    numInstances = getInformation(input);

    while (numInstances > 0) {
    	getGeneralInfo(input, &numStudents, &numUniversities, &maxPrefListSize);

        studentsTable = TableConstructor(numStudents);
        univTable = TableConstructor(numUniversities);

        getStudentsInfo(input, studentsTable, numStudents, maxPrefListSize);
        getUnivInfo(input, univTable, numUniversities);

        studentHeuristic(studentsTable, univTable, numStudents, numUniversities);

		int i;
		node *n;
		for (i = 0; i < numUniversities; i++) {
			printf("[%d]: %d\n", i + 1, univTable[i].info);
			printf("Aprovados:");
			for (n = begin(univTable[i].end); n != univTable[i].end; n =
					n->next) {
				printf(" %d", n->key);
			}
			printf("\n");
		}

        writeApproveds(output, univTable, numUniversities);
        writeSatisfactionMetrics(output, univTable, studentsTable, numUniversities);
        fprintf(output, "\r\n");

        resetTable(univTable, numUniversities);

        univHeuristic(studentsTable, univTable, numStudents, numUniversities);

        writeApproveds(output, univTable, numUniversities);
        writeSatisfactionMetrics(output, univTable, studentsTable, numUniversities);

        clearTable(studentsTable, numStudents);
        clearTable(univTable, numUniversities);

        numInstances--;

        if(numInstances != 0) {
            fprintf(output, "\r\n");
        }
    }

    fclose(input);
    fclose(output);
    return 0;
}
