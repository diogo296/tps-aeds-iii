#ifndef CREATEVECTOR_H_INCLUDED
#define CREATEVECTOR_H_INCLUDED

#include <stdlib.h>
#include "list.h"

void quickSort(float *vector, int left, int right);

float *createVector_percVacancies(List *univTable, int numUniv);

float *createVector_studSatisf(List *univTable, int numUniv, int vectorSize);

float *createVector_univSatisf(List univList, List *studentsTable);

float *createVector_cutoff(List *univTable, List *studentsTable, int numUniv);

#endif // CREATEVECTOR_H_INCLUDED
