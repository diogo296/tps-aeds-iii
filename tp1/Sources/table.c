#include "table.h"

List *TableConstructor(int tableSize) {
    List *table = (List*)malloc(sizeof(List)*tableSize);
    int i;
    for (i=0; i<tableSize; i++) {
        ListConstructor(&table[i]);
    }
    return table;
}

void resetTable(List *table, int tableSize) {
    int i;
    for (i = 0; i < tableSize; i++) {
        clear(&table[i]);
    }
}

void clearTable(List *table, int tableSize) {
    int i;
    for (i = 0; i < tableSize; i++) {
        clear(&table[i]);
        free(table[i].end);
    }
    free(table);
}
