#include "metrics.h"

float averagePercVacancies(List *univTable, int numUniv) {
    int i;
    float totalVacancies = 0, filledVacancies = 0;
    for (i = 0; i < numUniv; i++) {
        totalVacancies += univTable[i].info;
        filledVacancies += univTable[i].size;
    }
    return (totalVacancies/filledVacancies)*100;
}

float medianPercVacancies(List *univTable, int numUniv) {
    float aux1, aux2;
    float *vector = createVector_percVacancies(univTable, numUniv);

    if (numUniv % 2 != 0) {
        aux1 = vector[numUniv/2];
        free(vector);
        return (aux1*100);
    }
    else {
        aux1 = vector[(numUniv/2)-1];
        aux2 = vector[(numUniv/2)];
        free(vector);
        return (aux1 + aux2)*100/2;
    }
}

float averageStudentsSatisf(List *univTable, int numUniv) {
    float sumStudentsSatisf = 0, totalVacancies = 0;
    int i;
    node *n;
    for (i = 0; i < numUniv; i++) {
        totalVacancies += univTable[i].info;
        for (n = begin(univTable[i].end); n != univTable[i].end; n = n->next) {
            sumStudentsSatisf += n->weight;
        }
    }
    return (sumStudentsSatisf/totalVacancies);
}

float medianStudentsSatisf(List *univTable, int numUniv) {
    float aux1, aux2;
    int vectorSize = 0;
    int i;
    for (i = 0; i < numUniv; i++) {
        vectorSize += univTable[i].size;
    }

    float *vector = createVector_studSatisf(univTable, numUniv, vectorSize);

    if (numUniv % 2 != 0) {
        aux1 = vector[vectorSize/2];
        free(vector);
        return aux1;
    }
    else {
        aux1 = vector[(vectorSize/2)-1];
        aux2 = vector[(vectorSize/2)];
        free(vector);
        return (aux1 + aux2)/2;
    }
}

float averageUnivSatisf(List *univTable, List *studentsTable, int numUniv) {
    float sumStudentsGrade, averageSatisf = 0;
    int i;
    node *n;
    for (i = 0; i < numUniv; i++) {
        sumStudentsGrade = 0;
        for (n = begin(univTable[i].end); n != univTable[i].end; n = n->next) {
            sumStudentsGrade += studentsTable[n->key-1].info;
        }
        averageSatisf += (sumStudentsGrade/univTable[i].size);
    }
    return (averageSatisf/numUniv);
}

float medianUnivSatisf(List *univTable, List *studentsTable, int numUniv) {
    float aux1, aux2;
    float *vector1;

    float *vector2 = (float*)malloc(sizeof(float)*numUniv);

    int i;
    for (i = 0; i < numUniv; i++) {
        vector1 = createVector_univSatisf(univTable[i], studentsTable);
        if (univTable[i].size % 2 != 0) {
            aux1 = vector1[(univTable[i].size)/2];
            vector2[i] = aux1;
        }
        else  {
            aux1 = vector1[(univTable[i].size/2)-1];
            aux2 = vector1[(univTable[i].size/2)];
            vector2[i] = (aux1 + aux2)/2;
        }
        free(vector1);
    }

    quickSort(vector2, 0, numUniv-1);

    if (numUniv % 2 != 0) {
        aux1 = vector2[numUniv/2];
        free(vector2);
        return (aux1/2);
    }
    else {
        aux1 = vector2[(numUniv/2)-1];
        aux2 = vector2[(numUniv/2)];
        free(vector2);
        return (aux1 + aux2)/2;
    }
}

float averageCutoff(List *univTable, List *studentsTable, int numUniv) {
    int i;
    float cutoff, sumCutoff = 0;
    node *n;
    for (i = 0; i < numUniv; i++) {
        cutoff = studentsTable[front(univTable[i].end)-1].info;
        for(n = begin(univTable[i].end)->next; n != univTable[i].end; n = n->next) {
            if (cutoff > studentsTable[n->key-1].info) {
                cutoff = studentsTable[n->key-1].info;
            }
        }
        sumCutoff += cutoff;
    }
    return (sumCutoff/numUniv);
}

float medianCutoff(List *univTable, List* studentsTable, int numUniv) {
    float aux1, aux2;
    float *vector = createVector_cutoff(univTable, studentsTable, numUniv);

    if (numUniv % 2 != 0) {
        aux1 = vector[numUniv/2];
        free(vector);
        return (aux1/2);
    }
    else {
        aux1 = vector[(numUniv/2)-1];
        aux2 = vector[(numUniv/2)];
        free(vector);
        return (aux1 + aux2)/2;
    }
}
