#ifndef HEURISTICS_H_INCLUDED
#define HEURISTICS_H_INCLUDED

#include "table.h"

// Aprova um estudante em uma universidade.
void approve(List *studentsQueue, int univID, int prefWeight, List *studentsTable, List* univTable);

// Retira um estudante da universidade.
void putOffStudent(List *studentsQueue, List *studentsList, List *univList, node *student);

// Retorna TRUE se a universidade tem vagas.
int thereAreVacancies(List univList);

// Retorna TRUE se todas as vagas de todas as universidades est�o preenchidas
int allVacanciesFilled(List *univTable, int numUniv);

// Retorna TRUE se o estudante estiver avaliando sua �ltima prefer�ncia.
int isLastPreference(List studentList, int studentPref);

// Retorna TRUE se � a �ltima prefer�ncia do aluno de ID = studentID1 e n�o � a �ltima prefer�ncia do aluno de ID = studentID2.
int isVacancyPriority(int studentID1, int studentID2, int univID, List *studentsTable);

// Retorna TRUE se o aluno de ID = studentID tem maior prefer�ncia que o aluno de ID = student2->key.
int isPreferencePriority(int studentID, int prefWeight, node *student2, List *studentsTable);

// Retorna TRUE se o aluno de ID = studentID tem prefer�ncia de nota sobre o aluno de ID = student2->key.
int isGradePriority(int studentID, int prefWeight, node *student2, List *studentsTable);

// Caso a universidade prefira outro estudante, retorna o ponteiro para o n� do estudante j� aprovado
// que ser� reprovado ou o n� end caso contr�rio. Fun��o espec�fica da heuristica dos estudantes.
node *prefersOtherStudent_studHeur(int studentID, int univID, int prefWeight, List *studentsTable,
                                   List *univTable, int numUniv);

// Caso a universidade prefira outro estudante, retorna o ponteiro para o n� do estudante j� aprovado
// que ser� reprovado ou o n� end caso contr�rio. Fun��o espec�fica da heuristica das universidades.
node *prefersOtherStudent_univHeur(int studentID, int univID, int prefWeight, List *studentsTable,
                                   List *univTable, int numUniv);

// Constroi a fila de estudantes ainda n�o aprovados
// Inicialmente possui todos os IDs dos estudantes em ordem crescente.
void studentsQueueConstructor(List *studentsQueue, int numStudents);

// Heuristica que favorece os estudantes.
void studentHeuristic(List *studentsTable, List *univTable, int numStudents, int numUniv);

// Heuristica que favorece as universidades.
void univHeuristic(List *studentsTable, List *univTable, int numStudents, int numUniv);

#endif // HEURISTICS_H_INCLUDED
