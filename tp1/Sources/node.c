#include "node.h"

node* NewSentinel() {
    node* aux = (node*)malloc(sizeof(node));
    aux->next = aux->prev = aux;

    return aux;
}

node* NewNode(type k, node* left, node* right) {
    node* aux = (node*)malloc(sizeof(node));
    aux->key = k;
    aux->weight = 0;
    aux->next = right;
    aux->prev = left;

    return aux;
}
