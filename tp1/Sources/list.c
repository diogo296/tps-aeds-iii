#include "list.h"

void ListConstructor(List *list) {
    list->end = NewSentinel();
    list->size = 0;
}

node *begin(node *listEnd) {
    return listEnd->next;
}

type front(node *listEnd) {
    return begin(listEnd)->key;
}

node *find(type x, node *listEnd) {
    node *aux;

    for (aux = begin(listEnd); aux != listEnd; aux = aux->next) {
        if (x == aux->key) {
            return aux;
        }
    }

    return aux;
}

void erase(node *n, List *list) {
    n->prev->next = n->next;
    n->next->prev = n->prev;
    free(n);
    list->size--;
}

void popFront(List* list) {
    node *aux = begin(list->end);

    aux->next->prev = aux->prev;
    aux->prev->next = aux->next;

    free(aux);
    list->size--;
}

void pushBack(type x, List *list) {
    node *aux = NewNode(x, list->end->prev, list->end);
    list->end->prev->next = aux;
    list->end->prev = aux;
    list->size++;
}

void pushFront(type x, List *list) {
    node *aux = NewNode(x, list->end, list->end->next);
    list->end->next->prev = aux;
    list->end->next = aux;
    list->size++;
}

int empty(int listSize) {
    return (listSize == 0);
}

void clear(List *list) {
    while ( !empty(list->size) ) {
        popFront(list);
    }
}
