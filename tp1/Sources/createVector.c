#include "createVector.h"

void quickSort(float *vector, int left, int right) {
    int i = left, j = right;
    float pivot = vector[(left + right) / 2];

    while (i <= j) {
        while (vector[i] < pivot) {
            i++;
        }

        while (vector[j] > pivot) {
            j--;
        }

        if (i <= j) {
            float aux = vector[i];
            vector[i] = vector[j];
            vector[j] = aux;
            i++;
            j--;
        }
    }

    if (left < j) {
        quickSort(vector, left, j);
    }

    if (right > i) {
        quickSort(vector, i, right);
    }
}

float *createVector_percVacancies(List *univTable, int numUniv) {
    int i;
    float *vector = (float*)malloc(sizeof(float)*numUniv);
    for (i = 0; i < numUniv; i++) {
        vector[i] = (univTable[i].size / univTable[i].info);
    }

    quickSort(vector, 0, numUniv-1);

    return vector;
}

float *createVector_studSatisf(List *univTable, int numUniv, int vectorSize) {
    float *vector = (float*)malloc(sizeof(float)*vectorSize);

    int i = 0, j;
    node *n;
    for (j = 0; j < numUniv; j++) {
        for (n = begin(univTable[j].end); n != univTable[j].end; n = n->next) {
            vector[i] = n->weight;
            i++;
        }
    }

    quickSort(vector, 0, vectorSize-1);

    return vector;
}

float *createVector_univSatisf(List univList, List *studentsTable) {
    float *vector = (float*)malloc(sizeof(float)*univList.size);
    int i = 0;
    node *n;

    for (n = begin(univList.end); n != univList.end; n = n->next) {
        vector[i] = studentsTable[n->key-1].info;
        i++;
    }

    quickSort(vector, 0, univList.size-1);

    return vector;
}

float *createVector_cutoff(List *univTable, List *studentsTable, int numUniv) {
    float *vector = (float*)malloc(sizeof(float)*numUniv);

    int i;
    node *n;
    for (i = 0; i < numUniv; i++) {
        vector[i] = studentsTable[front(univTable[i].end)-1].info;
        for (n = begin(univTable[i].end)->next; n != univTable[i].end; n = n->next) {
            if (vector[i] > studentsTable[n->key-1].info) {
                vector[i] = studentsTable[n->key-1].info;
            }
        }
    }

    quickSort(vector, 0, numUniv-1);

    return vector;
}
