# Gerador de entrada para o tp0
# Como usar: python inputgen.py <NAME> <N> <MIN> <MAX>
# Onde:
# NAME: nome do arquivo a ser gerado
# N: 	quantidade de instancias geradas
# MIN: 	tamanho da primeira instancia
# MAX: 	tamanho maximo da instancia (os tamanhos gerados sao interpolados entre MIN e MAX)
# Exemplo: python inputgen.py input_10.txt 10 4 50
# 	Gera arquivo input_10.txt com 10 instancias, com tamanhos 4, 9, 14...49
# Rafael R. Cacique

import os;
import glob;
import random;
from sys import argv

# Input _________________________________________________
dummy, file_name, n_instances, min_size, max_size = argv;

n_instances = int(n_instances);
min_size = int(min_size);
max_size = int(max_size);
delta = 1 + (max_size - min_size) / n_instances # max((n_instances - 1), 1);
n = min_size;

out_file = open(file_name, "w");
out_file.write("%d\n" % n_instances);


for i in range(0, n_instances):
	# processando uma instancia: gera a lista [1,2,3...n]
	list = [k for k in range(1, n + 1)];
	# escreve o tamanho dos conjuntos
	out_file.write("%d\n" % n);
	# escreve as listas de preferencias dos homens e das mulheres
	for k in range(0, 2 * n):
		random.shuffle(list);
		list_str = ''.join(["%s " % (s) for s in list])
		list_str = list_str.strip();
		out_file.write("%s\n" % list_str);
	n = n + delta;

out_file.close();