#include "marriage2.h"

// Marca o casamento de um casal.
void getMarried(nodeKey man, int womanID, List *menTable, WomanArray *womenTable) {
    int womanWeight = begin(menTable[man.ID].end)->key.weight;
	constructNodeKey(&menTable[man.ID].married, womanID, womanWeight);
    constructNodeKey(&womenTable[womanID].married, man.ID, man.weight);
}

// Divorcia um casal.
void divorce(int womanID, List *manTable, WomanArray *womanTable, List *notMarriedList) {
    nodeKey divorced;

    constructNodeKey(&divorced, womanTable[womanID].married.ID, 0);

    constructNodeKey(&manTable[divorced.ID].married, 0, 0);
    constructNodeKey(&womanTable[womanID].married, 0, 0);

    pushBack(divorced, notMarriedList);
    popFront(&manTable[divorced.ID]);
}

// Retorna verdadeiro se a mulher está casada.
_Bool womanIsMarried(WomanArray woman) {
    return (woman.married.ID);
}

// Retorna verdadeiro se a mulher preferir o homem de ID = newHusbandID ao seu marido.
_Bool prefersOtherHusband(nodeKey newHusband, nodeKey oldHusband) {
	return (newHusband.weight < oldHusband.weight);
}

void createNotMarriedList(List *notMarriedList, int tableSize) {
	int i;
	nodeKey key;

	listConstructor(notMarriedList);

	for (i = 1; i <= tableSize; i++) {
		constructNodeKey(&key, i, 0);
		pushBack(key, notMarriedList);
	}
}

void menProposals(List notMarriedList, MenTable menTable, WomenTable *womenTable) {
	int manID, womanID;
	node *n;

	for (n = begin(notMarriedList.end); n != notMarriedList.end; n = n->next) {
		manID = n->key.ID;
		womanID = front(menTable.array[manID].end);
		womenTable->array[womanID].keys[manID].ID = true;
	}
}

nodeKey higherPrefMan(WomanArray *womanPreferences, int numPreferences, List *notMarriedList, List *menTable) {
	int i;
	nodeKey higherPref, aux;

	constructNodeKey(&higherPref, false, INT_MAX);

	for (i = 1; i <= numPreferences; i++) {
		if (womanPreferences->keys[i].ID != false) {
			if (womanPreferences->keys[i].weight < higherPref.weight) {
				if (higherPref.ID != false) {
					pushBack(higherPref, notMarriedList);
					popFront(&menTable[higherPref.ID]);
					womanPreferences->keys[higherPref.ID].ID = false;
				}

				constructNodeKey(&higherPref, i, womanPreferences->keys[i].weight);
			}

			else {
				womanPreferences->keys[i].ID = false;
				aux.ID = i;
				pushBack(aux, notMarriedList);
				popFront(&menTable[i]);
			}
		}
	}

	if (higherPref.ID != false) {
		womanPreferences->keys[higherPref.ID].ID = false;
	}

	return higherPref;
}

void chooseWomenHigherPref(List *notMarriedList, MenTable *menTable, WomenTable *womenTable) {
	int womanID;
	nodeKey womanPref;

	for (womanID = 1; womanID <= womenTable->size; womanID++) {
		womanPref = higherPrefMan(&womenTable->array[womanID], menTable->size, notMarriedList, menTable->array);

		if (womanPref.ID != false) {
			if (!womanIsMarried(womenTable->array[womanID])) {
				getMarried(womanPref, womanID, menTable->array, womenTable->array);
			}

			else if (prefersOtherHusband(womanPref, womenTable->array[womanID].married)) {
				divorce(womanID, menTable->array, womenTable->array, notMarriedList);
				getMarried(womanPref, womanID, menTable->array, womenTable->array);
			}

			else {
				pushBack(womanPref, notMarriedList);
				popFront(&menTable->array[womanPref.ID]);
			}
		}
	}
}

void findCouples(MenTable *menTable, WomenTable *womenTable) {
	List notMarriedList;
	pthread_t threads[4];

	createNotMarriedList(&notMarriedList, menTable->size);

	while (!empty(notMarriedList.size)) {
		menProposals(notMarriedList, *menTable, womenTable);
		resetList(&notMarriedList);
		chooseWomenHigherPref(&notMarriedList, menTable, womenTable);
	}
}

void marriage(FILE *input, FILE *output, int numInstances) {
    int numSubjects;
    MenTable menTable;
    WomenTable womenTable;

    while (numInstances > 0) {
        numSubjects = getInformation(input);

        // Inicializa a tabela dos homems e mulheres.
        constructAllTables(&menTable, &womenTable, numSubjects);

        // Guarda as preferências dos homens e mulheres.
        getAllSubjectsPreferences(input, &menTable, &womenTable);

        // Soulução do problema.
        findCouples(&menTable, &womenTable);

        // Escreve todos os dados no arquivo de saída.
        writeOutput(output, menTable, womenTable, numInstances);

        // Libera memória alocada
        clearAllTables(&menTable, &womenTable);

        numInstances--;
    }
}
