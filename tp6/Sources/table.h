#include "list.h"

#ifndef TABLE_H_INCLUDED
#define TABLE_H_INCLUDED

typedef struct {
	List *array;
	int size;
} MenTable;

typedef struct {
	nodeKey *keys;
	nodeKey married;
} WomanArray;

typedef struct {
	WomanArray *array;
	int size;
} WomenTable;

// Inicializa o vetor de listas de homens e mulheres.
void constructAllTables(MenTable *manTable, WomenTable *womanTable, int tableSize);

// Libera toda memória alocada pelos vetores de listas.
void clearAllTables(MenTable *manTable, WomenTable *womanTable);

void printTable(MenTable table);

#endif // TABLE_H_INCLUDED
