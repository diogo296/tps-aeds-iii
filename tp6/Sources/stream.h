#include <stdio.h>
#include <unistd.h>

#include "table.h"
#include "satisfaction.h"

#ifndef STREAM_H_INCLUDED
#define STREAM_H_INCLUDED

// Abre os arquivos de entrada e saída.
void openFiles(FILE **input, FILE **output, int argc, char *argv[]);

// Termina o programa caso um dos arquivos não seja aberto corretamente.
void checkFiles(FILE *input, FILE *output);

// Fecha os arquivos de entrada e saída.
void closeFiles(FILE *input, FILE *output);

int getInformation(FILE *input);

// Guarda todas as preferências dos homens e mulheres.
void getAllSubjectsPreferences(FILE *input, MenTable *menTable, WomenTable *womenTable);

// Escreve todos os dados no arquivo de saída.
void writeOutput(FILE *output, MenTable menTable, WomenTable womenTable, int numInstances);

#endif // STREAM_H_INCLUDED
