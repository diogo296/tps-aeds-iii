#include "table.h"

#ifndef SATISFACTION_H_INCLUDED
#define SATISFACTION_H_INCLUDED

// Calcula a satisfabilidade de um conjunto de homens ou mulheres.
float menSatisfaction(MenTable table);

float womenSatisfaction(WomenTable table);

#endif // SATISFACTION_H_INCLUDED
