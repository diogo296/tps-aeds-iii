#include "node.h"

node *NewSentinel() {
    node *aux = (node*) malloc(sizeof(node));
    aux->next = aux->prev = aux;

    return aux;
}

node *NewNode(nodeKey key, node *left, node *right) {
    node *aux = (node*) malloc(sizeof(node));
    aux->key = key;
    aux->next = right;
    aux->prev = left;

    return aux;
}

void constructNodeKey(nodeKey *key, keyType preference, int weight) {
	key->ID = preference;
	key->weight = weight;
}

void printNodeKey(nodeKey key) {
	printf("%d - %d || ", key.ID, key.weight);
}
