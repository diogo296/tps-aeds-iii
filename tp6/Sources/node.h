#ifndef NODE_H_INCLUDED
#define NODE_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>

typedef int keyType;

typedef struct {
	keyType ID;	// Conteúdo do nó.
	int weight;     // Peso do nó.
} nodeKey;

typedef struct Node {
    nodeKey key;        // Conteúdo do nó.
    struct Node *next;  // Apontador para o próximo nó.
    struct Node *prev;  // Apotador para o nó anterior.
} node;

// Cria um nó sentinela.
node *NewSentinel();

// Cria um novo nó.
node *NewNode(nodeKey key, node *left, node *right);

void constructNodeKey(nodeKey *key, keyType preference, int weight);

void printNodeKey(nodeKey key);

#endif // NODE_H_INCLUDED
