#include <limits.h>
#include <pthread.h>

#include "table.h"
#include "stream.h"

#ifndef MARRIAGE2_H_
#define MARRIAGE2_H_

void marriage(FILE *input, FILE *output, int numInstances);

#endif /* MARRIAGE2_H_ */
