#include "list.h"

void listConstructor(List *list) {
    list->end = NewSentinel();
    list->size = 0;
    constructNodeKey(&list->married, false, 0);
}

keyType front(node *listEnd) {
    return listEnd->next->key.ID;
}

node *find(keyType x, node *listEnd) {
    node *aux;

    for (aux = begin(listEnd); aux != listEnd; aux = aux->next) {
        if (x == aux->key.ID) {
            return aux;
        }
    }

    return aux;
}

node *begin(node *List_end) {
    return List_end->next;
}

void popFront(List *list) {
    node *aux = begin(list->end);

    aux->next->prev = aux->prev;
    aux->prev->next = aux->next;

    free(aux);
    list->size--;
}

void pushBack(nodeKey x, List *list) {
    node *aux = NewNode(x, list->end->prev, list->end);

    list->end->prev->next = aux;
    list->end->prev = aux;

    list->size++;
}

_Bool empty(int listSize) {
    return (listSize == 0);
}

void resetList(List *list) {
	while ( !empty(list->size) ) {
	    popFront(list);
	}
}

void clear(List *list) {
    resetList(list);
    free(list->end);
}

void printList(List list) {
	node *n;

	for (n = begin(list.end); n != list.end; n = n->next) {
		printNodeKey(n->key);
	}

	printf("\n");
}
