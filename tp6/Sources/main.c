#include <stdio.h>
#include <stdlib.h>

#include "marriage2.h"
#include "stream.h"

int main(int argc, char *argv[]) {
    int numInstances;
    FILE *input, *output;

    openFiles(&input, &output, argc, argv);

    checkFiles(input, output);

    numInstances = getInformation(input);

    marriage(input, output, numInstances);

    closeFiles(input, output);

    return 0;
}
