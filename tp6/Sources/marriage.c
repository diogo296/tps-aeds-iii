//#include "marriage.h"
//
//// Marca o casamento de um casal.
//void getMarried(int manID, int womanID, List *manTable, List *womanTable, List *notMarriedList) {
//    manTable[manID].married = womanID;
//    womanTable[womanID].married = manID;
//    popFront(notMarriedList);
//}
//
//// Divorcia um casal.
//void divorce(int womanID, List *manTable, List *womanTable, List *notMarriedList) {
//    nodeKey divorced;
//	constructNodeKey(&divorced, womanTable[womanID].married, 0);
//
//	manTable[divorced.ID].married = 0;
//    womanTable[womanID].married = 0;
//
//    pushBack(divorced, notMarriedList);
//}
//
//// Retorna verdadeiro se o homem/mulher está casado.
//_Bool isMarried(int ID, List *table) {
//    return (table[ID].married);
//}
//
//// Retorna verdadeiro se a mulher preferir o homem de ID = newHusbandID ao seu marido.
//_Bool prefersOtherHusband(int newHusbandID, List *womanPreference) {
//    node *n;
//
//    for (n = begin(womanPreference->end); n != womanPreference->end; n = n->next) {
//    	// Se o marido for encontrado na lista antes do novo pretendente.
//    	if (n->key.ID == womanPreference->married) {
//            return false;
//        }
//
//    	// Se o novo pretendente for encontrado na lista antes do marido.
//    	else if (n->key.ID == newHusbandID) {
//            return true;
//        }
//    }
//}
//
//void createNotMarriedList(List *notMarriedList, int tableSize) {
//	int i;
//	nodeKey key;
//
//	listConstructor(notMarriedList);
//
//	for (i = 1; i <= tableSize; i++) {
//		constructNodeKey(&key, i, 0);
//		pushBack(key, notMarriedList);
//	}
//}
//
//void updateManPreference(List notMarriedList, List *manTable, node **manPreference) {
//	int manID;
//
//	if (!empty(notMarriedList.size)) {
//		manID = front(notMarriedList.end);
//		*manPreference = begin(manTable[manID].end);
//	}
//}
//
//// Encontra e marca os casais.
//void findCouples(Table *manTable, Table *womanTable) {
//    int manID, womanID;
//    node *manPreference = begin(manTable->array[1].end);
//    List notMarriedList;
//
//    createNotMarriedList(&notMarriedList, manTable->size);
//
//    while (!empty(notMarriedList.size)) {
//    	manID = front(notMarriedList.end);
//        womanID = manPreference->key.ID;
//
//        // Se a mulher estiver livre:
//        if (!isMarried(womanID, womanTable->array)) {
//            getMarried(manID, womanID, manTable->array, womanTable->array, &notMarriedList);
//            updateManPreference(notMarriedList, manTable->array, &manPreference);
//        }
//
//        // Se a mulher preferir o homem de ID igual a manID ao seu marido:
//        else if (prefersOtherHusband(manID, &womanTable->array[womanID])) {
//            divorce(womanID, manTable->array, womanTable->array, &notMarriedList);
//            getMarried(manID, womanID, manTable->array, womanTable->array, &notMarriedList);
//            updateManPreference(notMarriedList, manTable->array, &manPreference);
//        }
//
//        // Caso o homem seja rejeitado:
//        else {
//            manPreference = manPreference->next;
//        }
//    }
//
//    clear(&notMarriedList);
//}
//
//void marriage(FILE *input, FILE *output, int numInstances) {
//    int numSubjects;
//    Table manTable, womanTable;
//
//    while (numInstances > 0) {
//        numSubjects = getInformation(input);
//
//        // Inicializa o vetor de listas dos homems e mulheres.
//        constructAllTables(&manTable, &womanTable, numSubjects);
//
//        // Guarda as preferências dos homens e mulheres.
//        getAllSubjectsPreferences(input, &manTable, &womanTable);
//
//        // Soulução do problema.
//        findCouples(&manTable, &womanTable);
//
//        // Escreve todos os dados no arquivo de saída.
//        writeOutput(output, manTable, womanTable, numInstances);
//
//        // Libera memória alocada
//        clearAllTables(&manTable, &womanTable);
//
//        numInstances--;
//    }
//}
