#include "satisfaction.h"

float menSatisfaction(MenTable table) {
    int i;
//    node* n;
    float total = 0;

    for (i = 1; i <= table.size; i++) {
    	total += table.array[i].married.weight;
//    	for (n = begin(table.array[i].end); n != table.array[i].end; n = n->next) {
//    		total++;
//            if (n->key.ID == table.array[i].married) {
//            	total += n->key.weight;
//                break;
//            }
//        }
    }

    return (total/table.size);
}

float womenSatisfaction(WomenTable table) {
    int i;
    float total = 0;

    for (i = 1; i <= table.size; i++) {
    	total += table.array[i].married.weight;
    }

    return (total/table.size);
}
