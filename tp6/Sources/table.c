#include "table.h"

// Inicializa e aloca memória para um vetor de listas.
void menTableConstructor(MenTable *table, int tableSize) {
	int i;

	table->array = (List*) malloc(sizeof(List) * (tableSize+1));
	table->size = tableSize;

    for (i = 1; i <= tableSize; i++) {
        listConstructor(&table->array[i]);
    }
}

void womenTableConstructor(WomenTable *womenTable, int tableSize) {
	int i, j;

	womenTable->size = tableSize;
	womenTable->array = (WomanArray*) malloc(sizeof(WomanArray) * (tableSize+1));

	for (i = 1; i <= tableSize; i++) {
		womenTable->array[i].keys = (nodeKey*) malloc(sizeof(nodeKey) * (tableSize+1));
	}

	for (i = 1; i <= tableSize; i++) {
		constructNodeKey(&womenTable->array[i].married, false, 0);
		for (j = 1; j <= tableSize; j++) {
			womenTable->array[i].keys[j].ID = false;
		}
	}
}

void constructAllTables(MenTable *manMenTable, WomenTable *womanMenTable, int tableSize) {
	menTableConstructor(manMenTable, tableSize);
	womenTableConstructor(womanMenTable, tableSize);
}

// Libera toda memória alocada por um vetor de listas.
void clearMenTable(MenTable *table) {
    int i;

    for (i = 1; i <= table->size; i++) {
        clear(&table->array[i]);
    }

    free(table->array);
}

void clearWomenTable(WomenTable *womenTable) {
	int i;

	for (i = 0; i <= womenTable->size; i++) {
		free(womenTable->array[i].keys);
	}

	free(womenTable->array);
}

void clearAllTables(MenTable *manMenTable, WomenTable *womanMenTable) {
	clearMenTable(manMenTable);
	clearWomenTable(womanMenTable);
}

void printMenTable(MenTable table) {
	int i;

	for (i = 1; i <= table.size; i++) {
		printList(table.array[i]);
	}

	printf("\n");
}
