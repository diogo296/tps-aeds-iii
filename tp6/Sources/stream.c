#include "stream.h"

void openFiles(FILE **input, FILE **output, int argc, char *argv[]) {
	char options[] = "i:o:";
	int option;

	while((option = getopt(argc, argv, options)) != -1) {
		switch(option) {
			case 'i':
		    	*input = fopen(optarg, "r");
	            break;
			case 'o':
				*output = fopen(optarg, "w");
	        	break;
		}
	}
}

void checkFiles(FILE *input, FILE *output) {
     if (!input || !output) {
    	 printf("Arquivo nao encontrado!\n");
    	 exit(1);
    }
}

void closeFiles(FILE *input, FILE *output) {
	fclose(input);
	fclose(output);
}

int getInformation(FILE* input) {
    int information;
    fscanf(input, "%d", &information);
    return information;
}

// Guarda todas as preferências dos homens.
void getMenPreferences(FILE *input, MenTable *menTable) {
    int i, j;
    nodeKey key;

    for (i = 1; i <= menTable->size; i++) {
        for (j = 1; j <= menTable->size; j++) {
            constructNodeKey(&key, getInformation(input), j);
            pushBack(key, &menTable->array[i]);
        }
    }
}

// Guarda todas as preferências das mulheres.
void getWomenPreferences(FILE *input, WomenTable *womenTable) {
    int i, j;
    int manID;

    for (i = 1; i <= womenTable->size; i++) {
        for (j = 1; j <= womenTable->size; j++) {
        	manID = getInformation(input);
            womenTable->array[i].keys[manID].weight = j;
        }
    }
}

void getAllSubjectsPreferences(FILE *input, MenTable *menTable, WomenTable *womenTable) {
	getMenPreferences(input, menTable);
	getWomenPreferences(input, womenTable);
}

// Escreve no arquivo de saída os casais formados.
void writeCouples(FILE *output, MenTable table) {
    int i;

    for (i = 1; i <= table.size; i++) {
        fprintf(output, "%d %d\r\n", i, table.array[i].married.ID);
    }
}

// Escreve no arquivo de saída a satisfabilidade dos homens, das mulheres e a média.
void writeSatisfaction(FILE *output, float manSatisfaction, float womanSatisfaction) {
    fprintf(output, "%.3f\n", (manSatisfaction + womanSatisfaction)/2);
    fprintf(output, "%.3f\n", manSatisfaction);
    fprintf(output, "%.3f", womanSatisfaction);
}

void writeOutput(FILE *output, MenTable manTable, WomenTable womanTable, int numInstances) {
	float manSatisfaction, womanSatisfaction;

	writeCouples(output, manTable);

	manSatisfaction = menSatisfaction(manTable);
	womanSatisfaction = womenSatisfaction(womanTable);

	writeSatisfaction(output, manSatisfaction, womanSatisfaction);

	if (numInstances > 1) {
		fprintf(output, "\n");
	}
}
