#include <stdbool.h>
#include <stdio.h>

#include "node.h"

#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED

typedef struct {
    node* end;    // Nó sentinela marcador do fim da lista.
    int size;     // Tamanho da lista.
    nodeKey married;  // Caso o indivíduo esteja casado, retorna o ID do parceiro. Caso contrário, retorna 0.
} List;

// Inicializa a lista.
void listConstructor(List *list);

// Retorna o valor da chave do primeiro elemento da lista em O(1).
keyType front(node *listEnd);

// Retorna o nó que contém a chave x em O(n).
node *find(keyType x, node *listEnd);

// Retorna um ponteiro para o primeiro node da lista em O(1).
node *begin(node *listEnd);

// Remove o primeiro elemento da lista em O(1).
void popFront(List *list);

// Insere x no final da lista em O(1).
void pushBack(nodeKey x, List *list);

// Retorna TRUE caso a lista seja vazia e FALSE caso contrário.
_Bool empty(int listSize);

void resetList(List *list);

// Remove todos os elementos da lista em O(n) e libera a memória alocada,
// onde n é o número de elementos na lista.
void clear(List *list);

void printList(List list);

#endif // LIST_H_INCLUDED
