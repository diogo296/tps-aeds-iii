#ifndef TRIANGULATION_H_
#define TRIANGULATION_H_

#include "Heuristics.h"

// Retorna TRUE se o primeiro ponto da fila pointsQueue forma uma orelha válida com seus adjacentes.
short isValidEar(Graph *polygon, List pointsQueue);

// Adiciona um triangulo feito na triangulação em um vetor de listas.
void addTriangle(List *triangles, typeKey ID1, typeKey ID2, typeKey ID3);

// Triangula um polígono.
void triangulate(Graph *graph, List *triangles);

#endif /* TRIANGULATION_H_ */
