#ifndef VERTEXESVISION_H_
#define VERTEXESVISION_H_

#include "Heuristics.h"

#define FALSE 0
#define TRUE  1

// Atualiza a fila de pontos.
void refreshQueue(List *pointsQueue);

// Retorna TRUE se o vértice de ID testedID está no campo de visão do vértice de ID point ID.
short isOnSight(Graph *polygon, typeKey pointID, typeKey testedID, List pointsQueue);

// Encontra o campo de visão de todos os vértices do polígono.
void setVertexesVision(Graph *polygon);

#endif /* VERTEXESVISION_H_ */
