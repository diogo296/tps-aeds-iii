#ifndef POINT_H_
#define POINT_H_

#include <stdio.h>

typedef double typeCoordenate;

typedef struct {
	typeCoordenate x;
	typeCoordenate y;
} Point;

// Define as coordenadas de um ponto.
void setCoordenates(Point *point, typeCoordenate x, typeCoordenate y);

// Retorna a orienrtação de 3 pointos (horário ou anti-horário).
short ccw(Point p0, Point p1, Point p2);

// Retorna TRUE se os pontos são iguais.
short equalPoints(Point p0, Point p1);

// Retorna TRUE se os pontos são colineares em relação às coordenadas y.
short collinear(Point p0, Point p1);

// Retorna TRUE se existe um ponto adjacente a p1 acima do mesmo e outro ponto adjacente a p1 abaixo do mesmo.
short isVertically(Point p0, Point p1, Point p2);

// Imprime as coordenadas de um ponto na tela.
void printPoint(Point point);

#endif /* POINT_H_ */
