#include "node.h"

node *NewSentinel() {
    node *aux = (node*)malloc(sizeof(node));
    aux->next = aux->prev = aux;
    return aux;
}

node *NewNode(typeKey ID, node *left, node *right) {
    node *aux = (node*)malloc(sizeof(node));
    aux->ID = ID;
    aux->prev = left;
    aux->next = right;
    return aux;
}
