#ifndef FIRSTHEURISTIC_H_
#define FIRSTHEURISTIC_H_

#include "Graph.h"
#include "Line.h"
#include "triangulation.h"
#include "graphColoration.h"
#include "vertexesVision.h"
#include "vertexesCover.h"

// Retorna TRUE se a linha testada intercepta alguma aresta do polígono.
short intersectPolygon(Graph *polygon, Line line1, List pointsQueue);

// Retorna TRUE se o ponto testado está dentro do polígono.
short isInsidePolygon(Graph *graph, Point point, List pointsQueue);

// Retorna a cor menos comum do grafo.
short findLowestColor(Graph *graph);

// Retorna uma lista com o número mínimo de guardas para vigiar o museu.
void firstHeuristic(Graph *graph, List *watchmen);

// Retorna uma lista com o número mínimo de guardas para vigiar o museu.
void secondHeuristic(Graph *polygon, List *watchmen);

#endif /* FIRSTHEURISTIC_H_ */
