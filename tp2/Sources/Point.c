#include "Point.h"

void setCoordenates(Point *point, typeCoordenate x, typeCoordenate y) {
	point->x = x;
	point->y = y;
}

short ccw(Point p0, Point p1, Point p2) {
    int dx1, dx2, dy1, dy2;

    dx1 = p1.x - p0.x;
    dy1 = p1.y - p0.y;

    dx2 = p2.x - p0.x;
    dy2 = p2.y - p0.y;

    if (dx1*dy2 > dy1*dx2) {
        return +1;
    }
    if (dx1*dy2 < dy1*dx2) {
        return -1;
    }
    if ((dx1*dx2 < 0) || (dy1*dy2 < 0)) {
        return -1;
    }
    if ((dx1*dx1 + dy1*dy1) < (dx2*dx2 + dy2*dy2)) {
        return +1;
    }
}

short equalPoints(Point p0, Point p1) {
	return ((p0.x == p1.x) && (p0.y == p1.y));
}

short collinear(Point p0, Point p1) {
	return ((p0.y == p1.y) && (p0.x > p1.x));
}

short isVertically(Point p0, Point p1, Point p2) {
	return ( ((p0.y > p1.y) && (p2.y < p1.y)) || ((p0.y < p1.y) && (p2.y > p0.y)) );
}
void printPoint(Point point) {
    printf("(%.0lf, %.0lf)\n", point.x, point.y);
}
