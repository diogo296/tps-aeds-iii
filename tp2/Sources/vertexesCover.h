#ifndef VERTEXESCOVER_H_
#define VERTEXESCOVER_H_

#include <stdlib.h>
#include "Graph.h"
#include "graphColoration.h"

// Retorna o nó da fila de vértices com grau mais alto, ou seja, o que possui mais adjacentes no grafo.
node *getHigherGradeVertex(Graph *graph, List vertexesQueue);

// Remove todos os vértices já coloridos, ou seja, todos os vértices já vistos por outros vértices.
void removeColoredVertexes(Graph *graph);

// Retorna os vértice pelos quais é possível acessar todos os outros vértices.
void vertexesCover(Graph *polyon, List *watchmen);

#endif /* VERTEXESCOVER_H_ */
