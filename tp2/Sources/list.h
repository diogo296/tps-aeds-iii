#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED

#include <stdio.h>
#include "node.h"
#include "Point.h"

typedef struct {
    node *end;    // Nó marcador do fim da lista.
    int size;     // Número de elementos da lista.
    Point point;  // Vértice da lista representado por um ponto 2D.
    int color;
} List;

// Inicializa uma lista vazia.
void ListConstructor(List *list);

// Cria uma lista com n elementos em ordem crescente, tal que n = listSize.
void createList(List *list, int listSize);

// Retorna um ponteiro para o primeiro nó da lista em O(1).
node *front(node *listEnd);

// Retorna um ponteiro para o último nó da lista em O(1).
node *back(node *listEnd);

// Retorna o valor do ID do primeiro elemento da lista em O(1).
typeKey first(node *listEnd);

// Retorna o valor do ID do segundo elemento da lista em O(1).
typeKey second(node *listEnd);

// Retorna o valor do ID do último elemento da lista em O(1).
typeKey last(node *listEnd);

// Retorna um vértice de certo ID na lista em O(n).
node *find(typeKey ID, node *listEnd);

// Remove o nó n da lista em O(1);
void erase(node *n, List *list);

// Remove o primeiro elemento da lista em O(1).
void popFront(List *list);

// Insere um vértice no final da lista em O(1).
void pushBack(typeKey ID, List *list);

// Insere um vértice no início da lista em O(1).
void pushFront(typeKey ID, List *list);

// Retorna TRUE caso a lista seja vazia e FALSE caso contrário.
int empty(int listSize);

// Remove todos os adjacentes da lista, exceto os dois primeiros.
void resetList(List *list);

// Remove todos os elementos da lista em O(n) e libera a memória alocada,
// onde n é o número de elementos na lista.
void clear(List *list);

// Imprime a lista na tela.
void printList(List list);

#endif // LIST_H_INCLUDED
