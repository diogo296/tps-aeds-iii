#include "graphColoration.h"

short isAdjTriangle(List triangle1, List triangle2) {
	node *ID1 = find(first(triangle2.end), triangle1.end);
	node *ID2 = find(second(triangle2.end), triangle1.end);
	node *ID3 = find(last(triangle2.end), triangle1.end);

	return ( ((ID1 != triangle1.end) && (ID2 != triangle1.end)) ||
			 ((ID1 != triangle1.end) && (ID3 != triangle1.end)) ||
			 ((ID2 != triangle1.end) && (ID3 != triangle1.end)) );
}

short isColored(List vertex) {
	return (vertex.color != 0);
}

short allVertexesColored(Graph *graph) {
	int i;
	for (i = 1; i <= graph->numVertexes; i++) {
		if (!isColored(graph->adj[i])) {
			return FALSE;
		}
	}
	return TRUE;
}

void colorCoveredVertexes(Graph *graph, int vertexID) {
	node *n;
	for (n = front(graph->adj[vertexID].end); n != graph->adj[vertexID].end; n = n->next) {
		graph->adj[n->ID].color = BLACK;
	}
	graph->adj[vertexID].color = BLACK;
}

typeKey notColoredVertex(Graph *polygon, List triangle) {
	node *n;
	for (n = front(triangle.end); n != triangle.end; n = n->next) {
		if (!isColored(polygon->adj[n->ID])) {
			return (n->ID);
		}
	}
}

short getMissingColor(Graph *polygon, List triangle) {
	short black = 0, white = 0, gray = 0;
	node *n;

	for (n = front(triangle.end); n != triangle.end; n = n->next) {
		if (polygon->adj[n->ID].color == BLACK) {
			black = 1;
		}
		else if (polygon->adj[n->ID].color == WHITE) {
			white = 1;
		}
		else if (polygon->adj[n->ID].color == GRAY) {
			gray = 1;
		}
	}

	if (!black) {
		return BLACK;
	}
	else if (!white) {
		return WHITE;
	}
	else {
		return GRAY;
	}
}

void colorAdjTriangleVertex(Graph *polygon, List triangle) {
	polygon->adj[notColoredVertex(polygon, triangle)].color = getMissingColor(polygon, triangle);
}

void createTrianglesQueue(List *trianglesQueue, List *triangles) {
	ListConstructor(trianglesQueue);
	int i;
	for (i = 1; !empty(triangles[i].size); i++) {
		pushBack(i, trianglesQueue);
	}
}

void colorGraph(Graph *graph, List *triangles) {
	int i, j;
	node *n, *aux;
	List trianglesQueue;

	createTrianglesQueue(&trianglesQueue, triangles);

	typeKey ID1 = first(triangles[1].end);
	typeKey ID2 = second(triangles[1].end);
	typeKey ID3 = last(triangles[1].end);

	graph->adj[ID1].color = BLACK;
	graph->adj[ID2].color = WHITE;
	graph->adj[ID3].color = GRAY;

	while (!empty(trianglesQueue.size)) {
		i = first(trianglesQueue.end);
		popFront(&trianglesQueue);

		for (n = front(trianglesQueue.end); n != trianglesQueue.end; n = n->next) {
			j = n->ID;
			if (isAdjTriangle(triangles[i], triangles[j])) {
				colorAdjTriangleVertex(graph, triangles[j]);
				pushFront(j, &trianglesQueue);

				aux = n;
				n = aux->prev;
				erase(aux, &trianglesQueue);
			}
		}
	}

	clear(&trianglesQueue);
}
