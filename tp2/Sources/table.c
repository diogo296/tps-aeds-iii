#include "table.h"

List *TableConstructor(int tableSize) {
    List *table = (List*)malloc(sizeof(List)*(tableSize+1));
    int i;
    for (i = 1; i <= tableSize; i++) {
        ListConstructor(&table[i]);
    }
    return table;
}

void resetTable(List *table, int tableSize) {
    int i;
    for (i = 1; i <= tableSize; i++) {
        resetList(&table[i]);
    }
}

void clearTable(List *table, int tableSize) {
    int i;
    for (i = 1; i <= tableSize; i++) {
        clear(&table[i]);
    }
    free(table);
}

void printTable(List *table, int tableSize) {
    int i;
    for (i = 1; i <= tableSize; i++) {
        printf("#Vertice %d: ", i);
        printList(table[i]);
    }
}
