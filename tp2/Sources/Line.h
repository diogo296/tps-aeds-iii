#ifndef LINE_H_INCLUDED
#define LINE_H_INCLUDED

#include "Point.h"

typedef struct {
    Point p1;
    Point p2;
} Line;

// Cria uma linha com os pontos p1 e p2.
void setLine(Line *line, Point p1, Point p2);

// Retorna TRUE se as linhas l1 e l2 se interceptam, exceto quando as mesmas tem um ponto em comum.
short intersect(Line l1, Line l2);

#endif // LINE_H_INCLUDED
