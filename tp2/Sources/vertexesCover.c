#include "vertexesCover.h"

node *getHigherGradeVertex(Graph *graph, List vertexesQueue) {
	node *n;
	node *higherGrade = front(vertexesQueue.end);

	for (n = front(vertexesQueue.end)->next; n != vertexesQueue.end; n = n->next) {
		if (graph->adj[n->ID].size > graph->adj[higherGrade->ID].size) {
			higherGrade = n;
		}
	}

	return higherGrade;
}

void removeColoredVertexes(Graph *graph) {
	int i;
	node *n;
	for (i = 1; i <= graph->numVertexes; i++) {
		for (n = front(graph->adj[i].end); n != graph->adj[i].end; n = n->next) {
			if (isColored(graph->adj[n->ID])) {
				erase(n, &graph->adj[i]);
			}
		}
	}
}

void vertexesCover(Graph *polygon, List *watchmen) {
	List vertexesQueue;
	node *higherGrade;
	int numCoveredVertexes = 0;

	createList(&vertexesQueue, polygon->numVertexes);

	while (numCoveredVertexes < polygon->numVertexes) {
		higherGrade = getHigherGradeVertex(polygon, vertexesQueue);
		pushBack(higherGrade->ID, watchmen);

		if (isColored(polygon->adj[higherGrade->ID])) {
			numCoveredVertexes += polygon->adj[higherGrade->ID].size;
		}
		else {
			numCoveredVertexes += polygon->adj[higherGrade->ID].size + 1;
		}

		colorCoveredVertexes(polygon, higherGrade->ID);
		erase(higherGrade, &vertexesQueue);
		removeColoredVertexes(polygon);
	}

	clear(&vertexesQueue);
}
