#include "Graph.h"

void GraphConstructor(Graph *graph, int graphSize) {
	graph->adj = TableConstructor(graphSize);
	graph->numVertexes = graphSize;
	graph->numEdges = 0;
}

void addEdge(Graph *graph, typeKey ID1, typeKey ID2) {
	pushBack(ID2, &graph->adj[ID1]);
	pushBack(ID1, &graph->adj[ID2]);
	graph->numEdges++;
}

void resetGraph(Graph *graph) {
	int i;

	resetTable(graph->adj, graph->numVertexes);

	for (i = 1; i <= graph->numVertexes; i++) {
		if (i != graph->numVertexes) {
		    addEdge(graph, i, i+1);
		}
		else {
		    addEdge(graph, i, 1);
		}
	}

	graph->numEdges = graph->numVertexes;
}

void printGraph(Graph graph) {
	printf("Numero de vertices: %d\n", graph.numVertexes);
	printf("Numero de arestas: %d\n\n", graph.numEdges);
	printTable(graph.adj, graph.numVertexes);
}
