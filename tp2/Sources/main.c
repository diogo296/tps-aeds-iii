#include <stdlib.h>

#include "stream.h"
#include "Heuristics.h"

int main(int argc, char *argv[])
{
    infoType numInstances, numVertexes;
    Graph polygon;
    List watchmen;

    FILE *input  = fopen(argv[2], "r");
    FILE *output = fopen(argv[4], "w");
    fileCheck(input, output);


    numInstances = getInformation(input);

    while (numInstances > 0) {
    	numVertexes = getInformation(input);

    	GraphConstructor(&polygon, numVertexes);

        getPolygon(input, &polygon);

        firstHeuristic(&polygon, &watchmen);

        fprintf(output, "#Heuristica1:\r\n");
        writeWatchmen(output, polygon, watchmen);

        clear(&watchmen);
        resetGraph(&polygon);

        secondHeuristic(&polygon, &watchmen);

        fprintf(output, "\r\n#Heuristica2:\r\n");
        writeWatchmen(output, polygon, watchmen);

    	clear(&watchmen);
    	clearTable(polygon.adj, polygon.numVertexes);

        numInstances--;

        // Para não imprimir nova linha após a última instância.
        if(numInstances != 0) {
            fprintf(output, "\r\n");
        }
    }

    fclose(input);
    fclose(output);

    return 0;
}
