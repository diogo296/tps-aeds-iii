#include "stream.h"

infoType getInformation(FILE *input) {
    infoType information;
    fscanf(input, "%d", &information);
    return information;
}

typeCoordenate getCoordenateInfo(FILE *input) {
	typeCoordenate x1 = getInformation(input);
	fgetc(input);
	typeCoordenate x2 = getInformation(input);
	return (x1/x2);
}

void getPointCoords(FILE *input, Point *point) {
    typeCoordenate x, y;
    x = getCoordenateInfo(input);
    y = getCoordenateInfo(input);
    setCoordenates(point, x, y);
}

void getPolygon(FILE *input, Graph *graph) {
	int i;
    // Adiciona todos os pontos do polígono (vértices do grafo) e arestas adjacentes.
    for (i = 1; i <= graph->numVertexes; i++) {
    	getPointCoords(input, &graph->adj[i].point);
    	if (i != graph->numVertexes) {
    		addEdge(graph, i, i+1);
    	}
    	else {
    		addEdge(graph, i, 1);
    	}
    }
}

void quickSort(int *vector, int left, int right) {
    int i = left, j = right;
    int pivot = vector[(left + right) / 2];

    while (i <= j) {
        while (vector[i] < pivot) {
            i++;
        }

        while (vector[j] > pivot) {
            j--;
        }

        if (i <= j) {
            int aux = vector[i];
            vector[i] = vector[j];
            vector[j] = aux;
            i++;
            j--;
        }
    }

    if (left < j) {
        quickSort(vector, left, j);
    }

    if (right > i) {
        quickSort(vector, i, right);
    }
}

typeKey *createVector(List list) {
	typeKey *vector;
	int i = 1;
	node *n;

	vector = (typeKey*)malloc(sizeof(typeKey)*(list.size+1));

	for (n = front(list.end); n != list.end; n = n->next) {
		vector[i] = n->ID;
		i++;
	}

	quickSort(vector, 1, list.size);

	return vector;
}

void writeWatchmanView(FILE *output, List watchman, typeKey watchmanID) {
	int i;
	typeKey *vector;

	vector = createVector(watchman);

	fprintf(output, "%d :", watchmanID);

	for (i = 1; i <= watchman.size; i++) {
		fprintf(output, " %d", vector[i]);
	}

	free(vector);
}

void writeWatchmen(FILE *output, Graph graph, List watchmen) {
    int i;
    typeKey *vector = createVector(watchmen);

    resetGraph(&graph);
    setVertexesVision(&graph);

    fprintf(output, "%d\r\n", watchmen.size);
    for (i = 1; i <= watchmen.size; i++) {
    	writeWatchmanView(output, graph.adj[vector[i]], vector[i]);
    	if (i != watchmen.size) {
    		fprintf(output, "\r\n");
    	}
    }

    free(vector);
}

void fileCheck(FILE *input, FILE *output) {
     if (!input || !output) {
    	abort();
    }
}
