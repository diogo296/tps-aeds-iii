#ifndef GRAPHCOLORATION_H_
#define GRAPHCOLORATION_H_

#include "Graph.h"

#define BLACK 1
#define WHITE 2
#define GRAY  3

#define FALSE 0
#define TRUE  1

// Retorna TRUE se os triangulos são adjacentes, ou seja, se possuem 2 vértices em comum.
short isAdjTriangle(List triangle1, List triangle2);

// Retorna TRUE se o vértice já está colorido.
short isColored(List vertex);

// Retorna TRUE se todos os vértices estão coloridos.
short allVertexesColored(Graph *graph);

// Colore da mesma cor os vértices adjacentes ao vértice de ID vertexID.
void colorCoveredVertexes(Graph *graph, int vertexID);

// Retorna o ID do vértice não colorido do triângulo.
// Pré-condição: 2 vértices do triângulo já estão coloridos.
typeKey notColoredVertex(Graph *polygon, List triangle);

// Retorna a cor que ainda não foi utilizada para colorir os vértices do triângulo.
// Pré-condição: 2 vértices do triângulo já estão coloridos.
short getMissingColor(Graph *polygon, List triangle);

// Colore um vértice de um triângulo adjacente a outro.
void colorAdjTriangleVertex(Graph *polygon, List triangle);

// Cria uma fila de triângulos contendo a posição no vetor de listas de triângulos em ordem crescente.
void createTrianglesQueue(List *trianglesQueue, List *triangles);

// Colore um polígono já triangulado com 3 cores.
void colorGraph(Graph *graph, List *triangles);

#endif /* GRAPHCOLORATION_H_ */
