#ifndef GRAPH_H_
#define GRAPH_H_

#include "table.h"

typedef struct {
    List *adj;        // Lista de adjacentes de um ponto.
    int numVertexes;  // Número de vértices do grafo.
    int numEdges;     // Número de arestas do grafo.
} Graph;

// Inicializa o grafo.
void GraphConstructor(Graph *graph, int graphSize);

// Adiciona uma aresta ao grafo, ou seja, adiciona o vértice vertex2 na lista de adjacência do vértice vertex1.
void addEdge(Graph *graph, typeKey ID1, typeKey ID2);

// Elimina as arestas adicionadas ao grafo, deixando apenas as arestas dos pontos adjacentes do polígono.
void resetGraph(Graph *graph);

// Imprime o grafo na tela.
void printGraph(Graph graph);

#endif /* GRAPH_H_ */
