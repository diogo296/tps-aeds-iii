#include "Line.h"

void setLine(Line *line, Point p1, Point p2) {
    line->p1 = p1;
    line->p2 = p2;
}

short commonPoints(Line l1, Line l2) {
	return (equalPoints(l1.p1, l2.p1) || equalPoints(l1.p1, l2.p2) ||
            equalPoints(l1.p2, l2.p1) || equalPoints(l1.p2, l2.p2));
}

short intersect(Line l1, Line l2) {
    return ( (((ccw(l1.p1, l1.p2, l2.p1) * ccw(l1.p1, l1.p2, l2.p2)) <= 0) &&
              ((ccw(l2.p1, l2.p2, l1.p1) * ccw(l2.p1, l2.p2, l1.p2)) <= 0) && !commonPoints(l1, l2)) );
}
