#ifndef NODE_H_INCLUDED
#define NODE_H_INCLUDED

#include <stdlib.h>

typedef int typeKey;

typedef struct Node {
    typeKey ID;             // Contém o ID do ponto.
    struct Node *next;  // Apontador para o próximo nó.
    struct Node *prev;  // Apotador para o nó anterior.
} node;

// Cria o nó sentinela.
node *NewSentinel();

// Cria um novo node.
node *NewNode(typeKey ID, node *left, node *right);

#endif // NODE_H_INCLUDED
