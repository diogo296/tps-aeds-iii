#include "triangulation.h"

short isValidEar(Graph *polygon, List pointsQueue) {
	Point point;
	Line line;

	setLine(&line, polygon->adj[second(pointsQueue.end)].point, polygon->adj[last(pointsQueue.end)].point);
	setCoordenates(&point, (line.p1.x + line.p2.x)/2, (line.p1.y + line.p2.y)/2);

	return (!intersectPolygon(polygon, line, pointsQueue) && isInsidePolygon(polygon, point, pointsQueue));
}

void addTriangle(List *triangles, typeKey ID1, typeKey ID2, typeKey ID3) {
	pushBack(ID1, triangles);
	pushBack(ID2, triangles);
	pushBack(ID3, triangles);
}

void triangulate(Graph *polygon, List *triangles) {
	typeKey pointID;
	List pointsQueue;
	int i = 1;

	createList(&pointsQueue, polygon->numVertexes);

    while (pointsQueue.size > 3) {
    	pointID = first(pointsQueue.end);

        if (isValidEar(polygon, pointsQueue)) {
        	addEdge(polygon, last(pointsQueue.end), second(pointsQueue.end));
        	addTriangle(&triangles[i], second(pointsQueue.end), pointID, last(pointsQueue.end));
        	i++;
        }
        else {
        	pushBack(pointID, &pointsQueue);
        }

        popFront(&pointsQueue);
    }

    addTriangle(&triangles[i], first(pointsQueue.end), second(pointsQueue.end), last(pointsQueue.end));

    clear(&pointsQueue);
}
