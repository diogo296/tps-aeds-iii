#include "Heuristics.h"

short intersectPolygon(Graph *polygon, Line line1, List pointsQueue) {
	node *n;
	Line line2;

	for (n = front(pointsQueue.end); n != pointsQueue.end; n = n->next) {
		if (n->next != pointsQueue.end) {
			setLine(&line2, polygon->adj[n->ID].point, polygon->adj[n->next->ID].point);
		}
		else {
			setLine(&line2, polygon->adj[n->ID].point, polygon->adj[first(pointsQueue.end)].point);
		}
		if (intersect(line1, line2)) {
			return TRUE;
		}
	}

	return FALSE;
}

short isInsidePolygon(Graph *graph, Point point, List pointsQueue) {
    int count = 0;
    node *n;
    Point maxPoint;
    Point p0, p1, p2;
    Line testLine, edgeLine;

    setCoordenates(&maxPoint, -1, point.y);
    setLine(&testLine, point, maxPoint);

    for (n = front(pointsQueue.end); n != pointsQueue.end; n = n->next) {
    	p1 = graph->adj[n->ID].point;

		if (n->next != pointsQueue.end) {
			p2 = graph->adj[n->next->ID].point;
		}
		else {
			p2 = graph->adj[first(pointsQueue.end)].point;
		}

		if (n->prev != pointsQueue.end) {
			p0 = graph->adj[n->prev->ID].point;
		}
		else {
			p0 = graph->adj[last(pointsQueue.end)].point;
		}

    	setLine(&edgeLine, p1, p2);

        if (intersect(testLine, edgeLine) || collinear(point, edgeLine.p1) || collinear(point, edgeLine.p2)) {
        	if (maxPoint.y != edgeLine.p1.y) {
        		count++;
        	}
        	else if (!isVertically(p0, p1, p2)) {
        		count++;
        	}
        }
    }

	return !(count % 2 == 0);
}

short findLowestColor(Graph *graph) {
	int black = 0, white = 0, gray = 0;
	int i;

	for (i = 1; i <= graph->numVertexes; i++) {
		if (graph->adj[i].color == BLACK) {
			black++;
		}
		else if (graph->adj[i].color == WHITE) {
			white++;
		}
		else if (graph->adj[i].color == GRAY) {
			gray++;
		}
	}

	if ((black <= white) && (black <= gray)) {
		return BLACK;
	}
	else if ((white <= black) && (white <= gray)) {
		return WHITE;
	}
	else {
		return GRAY;
	}
}

void firstHeuristic(Graph *polygon, List *watchmen) {
	List *triangles;
	short lowestColor;
	int i;

	triangles = TableConstructor(polygon->numVertexes);
	ListConstructor(watchmen);

	triangulate(polygon, triangles);

	colorGraph(polygon, triangles);

	lowestColor = findLowestColor(polygon);

	for (i = 1; i <= polygon->numVertexes; i++) {
		if (polygon->adj[i].color == lowestColor) {
			pushBack(i, watchmen);
		}
	}

	clearTable(triangles, polygon->numVertexes);
}

void secondHeuristic(Graph *polygon, List *watchmen) {
	setVertexesVision(polygon);

	ListConstructor(watchmen);

	vertexesCover(polygon, watchmen);
}
