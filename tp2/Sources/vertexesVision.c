#include "vertexesVision.h"

void refreshQueue(List *pointsQueue) {
	pushBack(first(pointsQueue->end), pointsQueue);
	popFront(pointsQueue);
}

short isOnSight(Graph *polygon, typeKey pointID, typeKey testedID, List pointsQueue) {
	Point point;
	Line line;

	if ((testedID == first(polygon->adj[pointID].end)) || (testedID == second(polygon->adj[pointID].end))) {
		return FALSE;
	}

	setLine(&line, polygon->adj[pointID].point, polygon->adj[testedID].point);
	setCoordenates(&point, (line.p1.x + line.p2.x)/2, (line.p1.y + line.p2.y)/2);

	return (!intersectPolygon(polygon, line, pointsQueue) && isInsidePolygon(polygon, point, pointsQueue));
}

void setVertexesVision(Graph *polygon) {
	int i, j;
	List pointsQueue;
	createList(&pointsQueue, polygon->numVertexes);

	for (i = 1; i < polygon->numVertexes; i++) {
		for (j = i+1; j <= polygon->numVertexes; j++) {
			if (isOnSight(polygon, i, j, pointsQueue)) {
				addEdge(polygon, i, j);
			}
		}
		refreshQueue(&pointsQueue);
	}

	clear(&pointsQueue);
}
