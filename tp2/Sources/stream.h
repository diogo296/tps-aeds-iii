#ifndef STREAM_H_INCLUDED
#define STREAM_H_INCLUDED

#include <stdio.h>
#include "Graph.h"
#include "vertexesVision.h"

typedef int infoType;

// Retorna uma informação lida do arquivo.
infoType getInformation(FILE *input);

// Le do arquivo as coordenadas de um ponto.
void getPointCoords(FILE *input, Point *point);

// Guarda as informações dos alunos.
void getPolygon(FILE *input, Graph *graph);

// Ordena o vetor contendo os ID's dos vigias ou dos vértices vigiados por um vigia.
void quickSort(int *vector, int left, int right);

// Cria um vetor que contém os ID's de uma lista em ordem crescente.
typeKey *createVector(List list);

// Escreve no arquivo de saída o campo de visão de um vigia.
void writeWatchmanView(FILE *output, List watchman, typeKey watchmanID);

// Escreve no arquivo de saída a visão de todos os vigias.
void writeWatchmen(FILE *output, Graph graph, List watchmen);

// Termina o programa caso um dos arquivos não seja aberto corretamente.
void fileCheck(FILE *input, FILE *output);

#endif // STREAM_H_INCLUDED
