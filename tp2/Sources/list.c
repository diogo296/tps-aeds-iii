#include "list.h"

void ListConstructor(List *list) {
    list->end = NewSentinel();
    list->size = 0;
    list->color = 0;
}

void createList(List *list, int listSize) {
	int i;

	ListConstructor(list);

	for (i = 1; i <= listSize; i++) {
		pushBack(i, list);
	}
}

node *front(node *listEnd) {
    return listEnd->next;
}

node *back(node *listEnd) {
    return listEnd->prev;
}

typeKey first(node *listEnd) {
    return front(listEnd)->ID;
}

typeKey second(node *listEnd) {
    return front(listEnd)->next->ID;
}

typeKey last(node *listEnd) {
    return back(listEnd)->ID;
}

node *find(typeKey ID, node *listEnd) {
    node *aux;

    for (aux = front(listEnd); aux != listEnd; aux = aux->next) {
        if (ID == aux->ID) {
            return aux;
        }
    }

    return aux;
}

void erase(node *n, List *list) {
    n->prev->next = n->next;
    n->next->prev = n->prev;
    free(n);
    list->size--;
}

void popFront(List* list) {
    node *aux = front(list->end);

    aux->next->prev = aux->prev;
    aux->prev->next = aux->next;

    free(aux);
    list->size--;
}

void pushBack(typeKey ID, List *list) {
    node *aux = NewNode(ID, back(list->end), list->end);
    back(list->end)->next = aux;
    list->end->prev = aux;
    list->size++;
}

void pushFront(typeKey ID, List *list) {
    node *aux = NewNode(ID, list->end, front(list->end));
    front(list->end)->prev = aux;
    list->end->next = aux;
    list->size++;
}

int empty(int listSize) {
    return (listSize == 0);
}

void resetList(List *list) {
	list->color = 0;

	while (!empty(list->size)) {
		popFront(list);
	}
}

void clear(List *list) {
    while ( !empty(list->size) ) {
        popFront(list);
    }
    free(list->end);
}

void printColor(short color) {
	if (color == 1) {
		printf("BLACK\n");
	}
	else if (color == 2) {
		printf("WHITE\n");
	}
	else if (color == 3) {
		printf("GRAY\n");
	}
	else printf("NO COLOR\n");
}

void printList(List list) {
    node *n;
    printPoint(list.point);
    printColor(list.color);
    printf("Size: %d\n", list.size);
    printf("Adjacentes: ");
    for (n = front(list.end); n != list.end; n = n->next) {
        printf("%d", n->ID);
        if (n != list.end->prev) {
            printf(" - ");
        }
    }
    printf("\n\n");
}
