set terminal postscript eps 28 enhanced color
set style fill solid 1.00 border
set encoding utf8
set output "poligono2.eps"
set style fill solid 1.00 border
set title "Polígono tp2 instance2"
set xrange[0:]
set yrange[0:]
set xlabel "x"
set ylabel "y"
plot 'entrada_tp2 (cópia).txt' using 1:2 notitle with linespoints pointsize 0.4 pt 7
