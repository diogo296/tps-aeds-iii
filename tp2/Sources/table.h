#ifndef TABLE_H_INCLUDED
#define TABLE_H_INCLUDED

#include "list.h"
#include <stdio.h>

// Inicializa o conjunto.
List *TableConstructor(int tableSize);

// Remove todos os elementos do conjunto.
void resetTable(List *table, int tableSize);

// Remove todos os elementos do conjunto e libera o espaço alocado pelo vetor.
void clearTable(List *table, int tableSize);

// Imprime o vetor de listas na tela.
void printTable(List *table, int tableSize);

#endif // TABLE_H_INCLUDED
