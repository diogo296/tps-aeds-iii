#include <stdio.h>

int main()
{
    FILE* output = fopen("tp2_instance100.txt", "w");
    int i, v, x, y, a, j = 1;
    printf("Insira a quantidade de instancias:\n");
    scanf("%d", &i);
    fprintf(output, "%d\n", i);

    while(j <= i) {
        printf("Insira o numero de vertices(maior que 3, menor que 1000 e par) da instancia %d:\n", j);
        scanf("%d", &v);

        while( v <= 4 || v > 1000 || v%2==1) {
            printf("O numero de vertices deve ser maior que 4, menor que 1000 e par\nInsira novamente:\n");
            scanf("%d", &v);
        }

        fprintf(output, "%d\n", v);
        for( a = 0; a < v/2; a++) {
            if(a%2 == 1) {
                x = (a*10)+1;
                y = 60;
            }
            else {
                x = (a*10)+1;
                y = 80;
            }
            fprintf(output, "%d/%d %d/%d\n", x, 1, y, 1);
        }

        x = x+10;
        for( a = 0; a < v/2; a++) {
            if(a%2 == 1) {
                x = x-10;
                y = 40;
            }
            else {
                x = x-10;
                y = 20;
            }
            fprintf(output, "%d/%d %d/%d\n", x, 1, y, 1);
        }

        j++;
    }

    fclose(output);

    return 0;
}
